\chapter{Proofs}
\section{Sparsity of attention mappings:  Proof of Proposition~\ref{prop:full_simplex}} 
\label{proof:full_simplex}

Recall that, by definition, 
$\mapo(\s)=\p$ iff. $\p \in \argmin_{\p' \in \Sd} \Omega(\p') - \DP{\s}{\p'}$. 
The Lagrangian associated with this minimization problem is
\begin{equation}
\mathcal{L}(\p, \bm{\mu}, \tau) = \Omega(\p) - \DP{\s + \bm{\mu}}{\p} + \tau (\bm{1}^\top \p - 1).
\end{equation}
Since $\relint \Sd \neq \varnothing$ (\eqnref{relint}), Slater's condition holds
\citep{slater}, thus the KKT conditions below are necessary and sufficient.
\begin{eqnarray}
\left\{
\begin{array}{l}
0 \in \partial_\p~\mathcal{L}(\p, \bm{\mu}, \tau) = 
\partial \O(\p) - \s - \bm{\mu} + \tau \bm{1},\\
\DP{\p}{\bm{\mu}} = 0,\\
\p \in \Sd, \,\, \bm{\mu} \ge 0,
\end{array}
\right.
\end{eqnarray}
where the addition and subtraction in the first condition are extended to sets.
  
For the forward implication, for a given $\p \in \Sd$, we seek $\s$ such that 
$(\p, \bm{\mu}, \tau)$ are a solution to the KKT conditions 
for some $\bm{\mu} \ge 0$ and $\tau \in \real$. 
We will show that such $\s$ exists by simply choosing 
$\bm{\mu}=\bm{0}$ and $\tau=0$. 
Those choices are dual feasible and guarantee that the slackness complementary condition is satisfied. 
In this case, we have from the first condition that 
$\s \in \partial \Omega(\p)$. 
Since $\partial \O(\p)$ is non-empty for any $\p \in \Sd$,
we can always find
$\s \in \real^d$ such that $(\p, \s)$ are a dual pair, i.e.,
$\p = \nabla \O^*(\s)$, which proves that $\nabla \O^*(\real^d) = \Sd$.  
Moreover, we can build infintely many such $\s$ for other dual variable
choices, showing that sparse solutions are not unlikely.

For the reverse implication, we must show that the subdifferential is nonempty.
By assumption, for any $\p\in\Sd$ there exists $\s \in \real^d$ such that 
$\mapo(\s)=\p$. Therefore, there must exist $\bm{\mu}$ and $\tau$ such that 
$(\p, \bm{\mu}, \tau)$ satisfy the KKT conditions.
From the first condition, $\s + \bm{\mu} - \tau\bm{1} \in \partial \O(\p)$,
completing the proof.


\section{Jacobian for general differentiable $\O$: Proof of Proposition~\ref{prop:Jacobian_diff_regul}}
\label{proof:Jacobian_diff_regul}

Recall that
\begin{equation}
\mapo(\s) = 
\argmin_{\p \in \Sd} f(\p),
\quad \text{where} \quad 
f(\p) \coloneqq
\gamma \O(\p) - \p^\tr \s.
\end{equation}
The optimum satisfies the fixed point iteration 
\citep[Section~4.2]{parikhboyd}
\begin{equation}
\p^\star = \projsimplex(\p^\star - \nabla f(\p^\star)).
\label{eq:fixed_point}
\end{equation}
Seeing $\p^\star$ as a function of $\s$, and $\projsimplex$ and $\nabla
f$ as functions of their inputs, we can apply the chain rule to
\eqref{eq:fixed_point} to obtain
\begin{equation}
    \bm{J}_{\mapo}(\s) = \bm{J}_{\projsimplex} \left(\p^\star - \nabla
    f(\p^\star)\right) \left(\bm{J}_{\mapo}(\s) - \bm{J}_{\nabla
f \circ \p^\star}(\s)\right).
\label{eq:fixed_point_chain_rule}
\end{equation}
Applying the chain rule once again to $\nabla f(\p^\star) = \gamma \nabla
\O(\p^\star) - \s$, we obtain
\begin{equation}
\begin{aligned}
\bm{J}_{\nabla f \circ \p^\star}(\s) &=
\gamma \bm{J}_{\nabla \O}(\p^\star) \bm{J}_{\mapo}(\s) - \bm{I} \\
&= \gamma \bm{H}_\O(\p^\star) \bm{J}_{\mapo}(\s) - \bm{I}. 
\end{aligned}
\end{equation}
Plugging 
this into \eqref{eq:fixed_point_chain_rule} and re-arranging, we obtain
\begin{equation}
(\bm{I} + \bm{A}(\bm{B} - \bm{I})) ~ \bm{J}_{\mapo}(\s) = \bm{A},
\end{equation}
where we defined the shorthands 
\begin{equation}
\begin{aligned}
\bm{A} &\coloneqq \bm{J}_{\projsimplex}(\p^\star -
\gamma \nabla \O(\p^\star) + \s) \\
\bm{B} &\coloneqq \gamma \bm{H}_\O(\p^\star).\\
\end{aligned}
\end{equation}

\section{Fusedmax and Oscarmax Jacobian: Proof of Proposition \ref{prop:structured_backward}}
\label{proof:structured_backward}

\paragraph{Proof outline.} Let $\bs{z}^\star = \proxtv(\s)$ or
$\proxoscar(\s)$.  We use the optimality conditions of $\proxtv$,
respectively $\proxoscar$
in order to express $\bs{z}^\star$ as an explicit
function of $\s$.  Then, obtaining the Jacobians of $\proxtv(\s)$ and
$\proxoscar(\s)$ follows by application of the chain rule to the two
expressions. We discuss the proof for points where $\proxtv$ and $\proxoscar$
are differentiable; on the (zero-measure) set of nondifferentiable points (i.e.\
where the group structure changes) we may take one of Clarke's generalized
gradients \citep{clarke_book}.

\paragraph{Jacobian of $\proxtv$.}

\begin{lemma}
    Let $\bm{z}^\star = \proxtv(\s) \in \real^d$ and $G^\star_i$ be the
    set of indices around $i$ with the same value at the optimum, as defined in
    \secref{sparseattn-fused-oscar}. Then, we have

    \begin{equation}
        z^\star_i = \frac{\sum_{j \in G^\star_i} \ss_j + \lambda(s_{a_i} -
        s_{b_i})}{|G^\star_i|},
        \label{eq:fused_fwd_expr}
    \end{equation}
    where $a_i = \min G^\star_i, b_i = \max G^\star_i$ are the boundaries
    of segment $G^\star_i$, and
    \begin{equation}
        s_{a_i} = \begin{cases}
            0 & \mbox{if } a = 1, \\
            \sign(z^\star_{{a_i}-1} - z^\star_i) & \mbox{if }a > 1 \\
        \end{cases} \qquad \text{and} \qquad s_{b_i} = \begin{cases}
            0 & \mbox{if } b = d, \\
            \sign(z^\star_i - z^\star_{b_i + 1}) & \mbox{if } b < d \\
        \end{cases}.
    \end{equation}
    \label{lemma:fused_fwd}
\end{lemma}

To prove Lemma~\ref{lemma:fused_fwd}, we make use of the
optimality conditions of the fused lasso proximity operator
\cite[Equation 27]{pathwise}, which state that $\bs{z}^\star$ satisfies
\begin{equation}
    z^\star_j - \ss_j + \lambda(t_j - t_{j + 1}) = 0
\label{eq:subgrad}
\end{equation}
where we denote
\begin{equation*}
    t_j \in \begin{cases}
        \{0\} & \mbox{if } i \in \{1, d\}, \\
        \{\sign(z^\star_j - z^\star_{j-1})\} & \mbox{if } z^\star_j \neq z^\star_{j-1}, \\
        [-1, 1] & \text{o.w}. \\
\end{cases}
\qquad \forall j \in [d].
\end{equation*}
The optimality conditions \eqref{eq:subgrad} form a system with unknowns
$z^\star_j, t_j$ for $j \in [d]$. To express $\bs{z}^\star$ as a function of
$\s$, we shall now proceed to eliminate the unknowns $t_j$.

Let us focus on a particular segment $G^\star_i$. For readability, we drop the
segment index $i$ and use the shorthands $z \coloneqq z^\star_i, a \coloneqq a_i,$
and $b \coloneqq b_i$. By definition, $a$ and $b$ satisfy
\begin{equation}
    z^\star_j = z \quad \forall a \leq j \leq b, \qquad 
    z^\star_{a - 1} \neq z ~ \text{if } a > 1, \qquad 
    z^\star_{b + 1} \neq z ~ \text{if } b < d. 
\end{equation}

It immediately follows from the definition of $t_j$ in \eqref{eq:subgrad} that 
\begin{equation}
t_a = \begin{cases}
    0 & \mbox{if } a = 1, \\
    \sign(z- z^\star_{a-1}) & \mbox{if }a > 1 \\
\end{cases}\quad\mbox{and}\quad
t_{b+1} = \begin{cases}
    0 & \mbox{if }b = d, \\
    \sign(z^\star_{b+1} - z) & \mbox{if }b < d \\
\end{cases}. 
\end{equation}
In other words, the unknowns $t_a$ and $t_b$ are already uniquely determined.
To emphasize that they are known, we introduce $s_a \coloneqq t_a$
and $s_b \coloneqq t_{b+1}$, leaving $t_j$ only unknown for $a < j \leq b$.

By rearranging the optimality conditions \eqref{eq:subgrad} we obtain the
recursion
\begin{equation}
    \lambda t_j = \ss_j - z + \lambda t_{j + 1} \quad \forall a \leq j \leq b.
    \label{eq:recur}
\end{equation}
We start with the first equation in the segment (at $j=a$), and unroll the
recursion until reaching the stopping condition $j=b$.
\begin{equation}
    \begin{aligned}
        \lambda s_a &= \ss_a - z + \lambda t_{a+1} \\
                    % &= x_a - z + x_{a+1} - z + \lambda t_{a+2} \\ 
                    &= \ss_a - z + \ss_{a+1} - z + \dots + \ss_b - z + \lambda s_b \\
                    &= \sum_{k=a}^{b} \ss_k - (b - a + 1)z + \lambda s_b
\end{aligned}
\end{equation}
Rearranging the terms, we obtain the expression
\begin{equation}
    z = \frac{\sum_{k=a}^{b} \ss_k + \lambda(s_b - s_a)}{b - a + 1}.
\end{equation}
Applying this calculation to each segment in $\bm{z}^\star$ yields the desired
result. \hfill $\square$

The proof of Proposition~\ref{prop:structured_backward} follows by applying
the chain rule to \eqref{eq:fused_fwd_expr}, noting that the groups $G^*_i$
are constant within a neighborhood of $\s$ (observation also used for OSCAR
in \citep{oscar}).  Therefore, for $\proxtv$,
\begin{equation}
    \partialfrac{z^\star_i}{\ss_j} = \frac{1}{|G^\star_i|}
    \left(\sum_{k \in G_i^\star} \partialfrac{\ss_k}{\ss_j} +
        \lambda \left( \partialfrac{s_b}{\ss_j} 
        -\partialfrac{s_a}{\ss_j}\right)  \right).
\end{equation}
Since $s_b$ and $s_a$ are either constant or sign functions w.r.t.\ $\s$,
their partial derivatives are $0$, and thus 
\begin{equation}
    \partialfrac{z^\star_i}{\ss_j} = \begin{cases}
        \frac{1}{|G^\star_i|} & \mbox{if } j \in G^\star_i, \\
        0 & \text{o.w.} \\ 
    \end{cases}.
\end{equation}
% \vspace{-0.4cm}

\paragraph{Jacobian of $\proxoscar$.}

\begin{lemma}(\cite[Theorem 1]{oscar_prox}, \cite[Proposition 3]{oscar_proof})
    Let $\bm{z}^\star = \proxoscar(\s) \in \mathbb{R}^d$ and $G^\star_i$ be the set of
    indices around $i$ with the same value at the optimum: 
    $G^\star_i = \{ j \in [d]: |z^\star_i| = |z^\star_j| \}.$ Then, we have

    \begin{equation}
        z^\star_i = \sign(\ss_i) \max \left( \frac{\sum_{j \in G^\star_i} |\ss_j|}{|G^\star_i|} -
        w_i, 0 \right),
        \label{eq:oscar_fwd_expr}
    \end{equation}
    \begin{equation}
    \text{where~}
    w_i = \lambda\left(d - \frac{u_i + v_i}{2}\right), \quad
    u_i = \left| \{j \in [d]: |z^\star_j| < |z^\star_i| \} \right|, \quad
    v_i = u_i + |G^\star_i|.
    \end{equation}
    \label{lemma:oscar_fwd}
\end{lemma}

Lemma~\ref{lemma:oscar_fwd} is a simple reformulation of Theorem 1, part
\textit{ii} from \citep{oscar_prox}.  With the same observation that the induced
groups do not change within a neighborhood of $\s$, we may differentiate
\eqref{eq:oscar_fwd_expr} to obtain
\begin{equation}
    \partialfrac{z^\star_i}{\ss_j} = \begin{cases}
        0 & \mbox{if } z^\star_i = 0, \\

        \displaystyle\frac{\sign(\ss_i)}{|G^\star_i|}
    \sum_{k \in G_i^\star} \partialfrac{|\ss_k|}{\ss_k} \partialfrac{\ss_k}{\ss_j} -
    \partialfrac{w_i}{\ss_j} & \text{o.w.}
\end{cases}.
\end{equation}

Noting that $\partialfrac{w_i}{\ss_j}=0$, as $w_i$ is derived only from group
indices and the term $\partialfrac{|\ss_k|}{\ss_k} \partialfrac{\ss_k}{\ss_j}$ either
vanishes (when $k \neq j$) or else equals $\sign(x_j)$ with $\ss_j \neq 0$, we 
substitute $\sign(z_j^\star)$ for $\sign(\ss_j)$ \citep{oscar_prox}
to get
\begin{equation}
\partialfrac{z^\star_i}{\ss_j} = \begin{cases}
    \displaystyle\frac{\sign(z^\star_i z^\star_j)}{|G_i^\star|}
        & \mbox{if } j \in G_i^\star \mbox{ and } z_i^\star \neq 0, \\
    0
        & \mbox{o.w. }
\end{cases}.
\end{equation}

\section{Computing the SparseMAP Jacobian: Proof of Proposition~\ref{prop:backw}}
\label{proof:sparsemap-jacobian}

Recall that {\smap} is defined as the $\pu^\star$ that maximizes the value
of the quadratic program (\eqnref{smap}),
\begin{equation}
\label{eqn:qp_small}
g(\sru, \srv) \coloneqq \max_{[\pu;\pv]\in\marg} \sru^\tr \pu + \srv^\tr \pv
-\frac{1}{2}\norm{\pu}^2_2.
\end{equation}

As the $\ell_2^2$ norm is strongly convex, there is always a unique minimizer
$\pu^\star$ (implying that $\smap$ is well-defined), and the convex conjugate
of the QP in (\ref{eqn:qp_small}), $g^*(\pu, \pv)=\bigl\{ \frac{1}{2}\norm{\pu}_2^2,
[\pu;\pv]\in\marg; -\infty \text{ otherwise}\bigr \}$  is smooth in $\bs{u}$,
implying that $\smap$ (which only returns $\bs{u}$) is Lipschitz-continuous and thus
differentiable almost everywhere.

We now rewrite the QP in Equation~\ref{eqn:qp_small} in terms of the convex
combination of vertices of the marginal polytope
\begin{equation}
    \label{eq:regmin}
    \min_{\p\in\Simplex^D} \frac{1}{2}\norm{\MM\p}^2_2 - \s^\tr\p
    \qquad \text{where}~
    \s \coloneqq \AA^\tr\sr
\end{equation}

We use the optimality conditions of problem \ref{eq:regmin} to derive an
explicit relationship between $\bs{u}^\star$ and $\bs{x}$.
At an optimum, the following KKT conditions hold
\begin{align}
    \MM^\tr\MM\p^\star - \bs{\lambda}^\star + \tau^\star\bs{1} &= \s\\
    \bs{1}^\tr\p^\star &= 1 \\
    \p^\star &\geq \bs{0} \\
    \bs{\lambda}^\star &\geq \bs{0} \\ 
    \bs{\lambda}^{\star \tr}\p^\star &= 0 \label{eq:slack}
\end{align}
Let $\supp$ denote the support of $\p^\star$, \ie, $\supp = \{\yy~:~\pp^\star_\yy > 0 \}$.
From Equation~\ref{eq:slack} we have $\bs{\lambda}_\supp = \bs{0}$ and therefore
\begin{align}
    {\MM_\supp}^\tr\MM_\supp\p_\supp^\star + \tau^\star\bs{1} &=
    \s_\supp \label{eq:sub1}\\
    \bs{1}^\tr\p_\supp^\star &= 1 \label{eq:sub2}
\end{align}
Solving for $\p_\supp^\star$ in Equation~\ref{eq:sub1} we get a direct expression 
\[
    {\p_\supp}^\star =
    ({\MM_\supp}^\tr
    \MM_\supp)^{-1}
    (\s_\supp - \tau^\star \bs{1}) 
    =
    \bs{Z}(\s_\supp - \tau^\star \bs{1}).
\]
where we introduced $\bs{Z}=(\MM_\supp^\tr\MM_\supp)^{-1}$. Solving for
$\tau^\star$ yields
\[
    \tau^\star = \frac{1}{\bs{1}^T \bs{Z} \bs{1}} \left(
    \bs{1}^T \bs{Z} \s_\supp- 1 \right)\\
\]
Plugging this back and left-multiplying by $\MM_\supp$ we get
\[
    \pu^\star = 
    \MM_\supp \p_\supp = \MM_\supp \bs{Z}\left(\s_\supp - \frac{1}{\bs{1}^\tr \bs{Z1}} \bs{1}^\tr
    \bs{Z}\s_\supp \bs{1} + \frac{1}{\bs{1}^\tr \bs{Z1}} \bs{1}\right)
\]
Note that, in a neighborhood of $\sr$, the support of the solution $\supp$ is
constant. (On the measure-zero set of points where the support changes, $\smap$
is subdifferentiable and our assumption yields a generalized
Jacobian~\citep{clarke_book}.)  Differentiating w.r.t.\ the score of a
configuration $\ss_\yy$, we get the expression
\begin{equation}
    \pfrac{\pu^\star}{\ss_\yy} = 
    \begin{cases}
        \MM\left(\bs{I} - \frac{1}{\bs{1}^T \bs{Z} \bs{1}}
        \bs{Z}\bs{1}\bs{1}^T\right) \col{Z}{\yy} & \yy \in \supp \\
        \bs{0} & \yy \notin \supp \\
    \end{cases}
\end{equation} 
Since $\ss_\yy = \col{\AA}{\yy}^\tr \sr$, by the chain rule, we get the desired
result
\begin{equation}
\pfrac{\pu^\star}{\sr} = \pfrac{\pu^\star}{\s} \AA^\tr.
\end{equation}

\section{Distribution-wise SparseMAP Jacobian: Proof of Proposition~\ref{prop:latent-backward}}
\label{proof:latent-backward}

We follow the derivation from Appendix~\ref{proof:sparsemap-jacobian}, with
the latent-variable notation from \chapref{latent}.
Any solution $p_\parp$ satisfies, for any
$\yy\in\bar\trees(x)$
\begin{equation}
    p_\parp(\yy | x) = \sum_{\yy' \in \bar\trees(x)} z_{\yy,\yy'}(\score(\yy'; x) -
    \tau^\star), 
\end{equation}
where
\begin{equation}
    \tau^\star = \frac{
-1+\sum_{\{\yy'',\yy'\}\in\bar\trees(x)} z_{\yy'',\yy'} \score(\yy'; x)
}{
% \sum_{\{h'',h'\}\in\bar\trees{x}} z_{h'',h'}
\zeta
}.
\end{equation}
To simplify notation, we denote
$p_\parp(\yy | x) = p_1(\parp) - p_2(\parp)$ where
\begin{equation}
\begin{aligned}
    p_1(\parp) &\coloneqq \sum_{\yy' \in \bar\trees(x)} z_{\yy,\yy'}~f_\parp(\yy'; x), \\
    p_2(\parp) &\coloneqq (\sum_{\yy'} z_{\yy,\yy'}) \cdot \tau^\star \\
    &= 
    \varsigma(\yy) \cdot 
    \zeta^{-1}\bigl(\sum_{\yy'',\yy'}z_{\yy'',\yy'}f_\parp(\yy'; x)\bigr) - \text{const}.\\
\end{aligned}
\end{equation}
Differentiating, we get
\begin{equation}
\begin{aligned}
\pfrac{p_1}{\parp} &= \sum_{\yy' \in \bar\trees(x)}
    z_{\yy,\yy'} \pfrac{\score(\yy'; x)}{\parp}, \\
%
\pfrac{p_2}{\parp} &= \sum_{\yy' \in \bar\trees(x)}
\varsigma(\yy) \cdot \zeta^{-1}\bigl(\sum_{\yy''}z_{\yy'', \yy'}\bigr)
\pfrac{\score(\yy'; x)}{\parp}. \\
&= \sum_{\yy' \in \bar\trees(x)}
\varsigma(\yy) \cdot \zeta^{-1} \varsigma(\yy')
\pfrac{\score(\yy'; x)}{\parp}. \\
\end{aligned}
\end{equation}
Putting it all together, we obtain
\begin{equation}
    \pfrac{p_\parp(\yy | x)}{\parp} =
    \sum_{\yy' \in \bar\trees(x)} 
    \left( z_{\yy,\yy'} -
    \zeta^{-1}
    \varsigma(\yy)\varsigma(\yy')
    % (\bs{1}^\tr \bs{z}_h) \cdot (\bs{1}^\tr \bs{z}_{h'})
    \right) \pfrac{\score(\yy')}{\parp},
\end{equation}
which is the top branch of the conditional.
For the other branch, observe that the support
$\bar\trees(x)$ is constant within a neighborhood of $\parp$, 
yielding $\yy\notin\bar\trees(x), \pfrac{p(\yy)}{\parp} = 0$.
Importantly, since $\bs{Z}$ is computed as a side-effect of the \smap forward
pass, the backward pass computation is efficient.

\section{Properties of \FY Losses: Proof of Proposition~\ref{prop:omega}}
\label{proof:loss}

We recall that the structured Fenchel-Young loss defined by a convex
$\Omega:\mathbb{R}^D\rightarrow \mathbb{R}$ and a matrix $\bs{A}$ is defined as
\[
\LL_\O^\AA : \real^d \times \Simplex^D \rightarrow \real_+,
\qquad
    \LL_\O^\AA(\sr, \gdp)\coloneqq
    \O_\Simplex^*(\AA^\tr\sr) + \Omega_\Simplex(\gdp) -
    \sr^\tr\AA\gdp.
\]
% Let $\widetilde{\Omega}:\mathbb{R}^D \rightarrow \mathbb{R}$ be the {\em
% extended-value extension} of
% $\Omega$ to $\mathbb{R}^D$\citep[Section 3.1.2]{boyd}, \ie,
% \[
    % \widetilde{\Omega}(\bs{y}) \coloneqq \begin{cases}
        % \Omega(\bs{y}) & \bs{y} \in \Simplex^D  \\
        % \infty & \bs{y} \notin \Simplex^D \\
    % \end{cases}
% \]
% \mb{Boyd uses the tilde notation but $\Omega_\Simplex$ could be nice too}
Since $\Omega_\Simplex$ is the restriction of a convex function to a convex set,
it is convex~\citep[Section 3.1.2]{boyd}.
% is convex, so is $\widetilde\Omega$. The convex conjugate of
% $\widetilde\Omega$ is exactly the regularized $\max$ operator~\citep{sparseattn}
% \[
    % \maxop(\pt) \coloneqq \max_{\bs{y} \in \Simplex^d} \pt^\tr \bs{y} -
    % \Omega(\bs{y}) = \widetilde{\Omega}^\star(\pt)
% \]

\paragraph{Property 1.}
From the Fenchel-Young inequality~\citep[Section 3.3.2]{fenchel,boyd}, we have
\[
    \s^\tr\p \leq \O_\Simplex^*(\s) + \O_\Simplex(\p).
\]
In particular, when $\sr = \AA^\tr\s$, % and $\bs{y} \in \Simplex^D$,
\[
  \begin{aligned}
      0 & \leq - \sr^\tr\AA\p + \O_\Simplex^*(\AA^\tr\sr) + \O_\Simplex(\p) \\
        & = \LL_\O^\AA(\sr, \p).
  \end{aligned}
\]

\paragraph{Property 2.}
Equality is achieved when
\[
    \begin{aligned}
    \O_\Simplex^*(\AA^\tr\sr)
    &= \sr^\tr\AA\p - \O_\Simplex(\p) \iff \\
    \max_{\p' \in \Simplex^D} \sr^\tr\AA \p' - \O(\p') &= 
    \sr^\tr\AA\p - \O(\p),\\
    \end{aligned}
\]
where we used the fact that $\p\in\Simplex^D$. The claim immediately follows.

\paragraph{Property 3.}
To prove convexity in $\sr$, we rewrite the loss, for fixed $\p$, as 
\[
    \LL_\O^\AA(\sr) = h(\AA^\tr\sr) + \text{const},
    \quad\text{where}\quad
    h(\s) = \O_\Simplex^*(\s) - \s^\tr\p.
\]
$\Omega_\Simplex^\star$ is a convex conjugate, and thus itself convex.
Linear functions are convex, and the sum of two convex functions is convex,
therefore $h$ is convex.
Finally, the composition of a convex function with a linear function
is convex as well, thus the function
$\left(h\bs{A}^\tr\right)$ is convex.  Convexity of $\LL_\O^\AA$ in
$\sr$ directly follows.
Convexity in $\bs{y}$ is straightforward, as the sum of a convex and a linear
function \citep[Sections 3.2.1, 3.2.2, 3.3.1]{boyd}.

% \paragraph{Property 4.} TODO

\paragraph{Property 4.}
This follows from the scaling property of the convex conjugate \citep[Section 3.3.2]{boyd}
\[(t\O)^*(\s) = t\O^*(t^{-1}\s)\]
Denoting $\sr' = t^{-1}\sr$, we have that
\[
\begin{aligned}
  \LL_{t\O}^\AA(\sr, \p)
  &= (t{\O_\Simplex})^*(\AA^\tr\sr)+t\O_\Simplex(\p)-\sr^\tr\AA\p\\
  &= t\O_\Simplex^*(\AA^\tr\sr')+t\O_\Simplex(\p)-\sr^\tr\AA\p\\
  &= t\bigl(\O_\Simplex^*(\AA^\tr\sr')+\O_\Simplex(\p)-\sr'^\tr\AA\p \bigr)
  = t \LL_\O^\AA(t^{-1}\sr, \p).
\end{aligned}
\]
