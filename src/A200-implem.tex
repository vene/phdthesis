\chapter{Implementation details}
\section{Conditional Gradient Algorithms for \smap}
\label{appendix:impl-cg}

We adapt the presentation of vanilla, away-step and pairwise conditional gradient
of \citet{cg}.

Recall the {\smap} problem (Equation~\ref{eqn:smap}), which we
rewrite below as a minimization, to align with the notation of \citet{cg}
\[
    \operatorname{\smap}_{\AA}(\sr)\coloneqq
    \argmin_{\pu:~[\pu,\pv] \in \marg}
    f(\pu,\pv), \qquad \text{where}~f(\pu,\pv)\coloneqq
    \frac{1}{2} \norm{\pu}_2^2
    -\sru^\tr\pu - \srv^\tr\pv.
\]
The gradients of the objective function $f$  w.r.t.\ the two variables are
\[
    \nabla_{\pu} f(\pu', \pv') = \pu' - \sru, \qquad 
    \nabla_{\pv} f(\pu', \pv') = - \srv.
\]
The ingredients required to apply conditional gradient algorithms are solving
linear minimization problem, selecting the away step, computing the Wolfe gap,
and performing line search.

\paragraph{Linear minimization problem.} For {\smap}, this amounts to a MAP
inference call, since
\[
\begin{aligned}
     & \argmin_{[\pu,\pv] \in \marg}
    ~ \bigl\langle \nabla_{\pu} f(\pu', \pv'), \pu \bigr\rangle
    + \bigl\langle \nabla_{\pv} f(\pu', \pv'), \pv \bigr\rangle\\
    =& \argmin_{[\pu,\pv] \in \marg}
    ~ (\pu' - \sru)^\tr \pu - \srv^\tr\pv \\ 
    =&~\{[\col{M}{\yy}, \col{N}{\yy}]~:~ \yy \in
        \map_\AA(\sru-\pu', \srv)\}.
\end{aligned}
\]
where we assume $\map_\AA$ yields the set of maximally-scoring structures.

\paragraph{Away step selection.} This step involves searching the currently
selected structures in the active set $\supp$ with the {\em opposite}
goal: finding the structure {\em maximizing} the linearization
\[
\begin{aligned}
    & \argmax_{\yy \in \supp}
    ~ \bigl\langle \nabla_{\pu} f(\pu', \pv'), \col{M}{\yy} \bigr\rangle
    + \bigl\langle \nabla_{\pv} f(\pu', \pv'), \col{N}{\yy} \bigr\rangle\\
    =& \argmax_{\yy \in \supp}
    ~ (\pu' - \sru)^\tr \col{M}{\yy} - \srv^\tr\col{N}{\yy} \\ 
\end{aligned}
\]

\paragraph{Wolfe gap.}
The gap at a point $\bs{d} = [\bs{d}_{\pu}, \bs{d}_{\pv}]$ is given by
\begin{equation}
\label{eqn:gap-app}
\begin{aligned}
    \operatorname{gap}(\bs{d}, \pu') &\coloneqq
        \bigl\langle -\nabla_{\pu} f(\pu', \pv'), \bs{d}_{\pu} \bigr\rangle
        + \bigl\langle -\nabla_{\pv} f(\pu', \pv'), \bs{d}_{\pv} \bigr\rangle\\
        &= \bigl \langle \sru - \pu', \bs{d}_{\pu}\bigr\rangle +
           \bigl \langle \srv, \bs{d}_{\pv}\bigr\rangle.\\
\end{aligned}
\end{equation}

\paragraph{Line search.}
Once we have picked a direction $\bs{d}=[\bs{d}_{\pu}, \bs{d}_{\pv}]$, we can pick
the optimal step size by solving a simple optimization problem.
Let $\pu_\gamma \coloneqq  \pu' + \gamma\bs{d}_{\pu}$,
and $\pv_\gamma \coloneqq  \pv' + \gamma\bs{d}_{\pv}$.
We seek $\gamma$ so as to optimize
\[
    \argmin_{\gamma \in [0, \gamma_{\max}]} f(\pu_\gamma,
    \pv_\gamma)
\]
Setting the gradient w.r.t.\ $\gamma$ to $0$ yields
\[
\begin{aligned}
0 &= \pfrac{}{\gamma} f(\pu_\gamma, \pv_\gamma)\\
  &= \bigl \langle \bs{d}_{\pu},\nabla_{\pu} f(\pu_\gamma, \pv_\gamma)
     \bigr \rangle +
     \bigl \langle \bs{d}_{\pv},\nabla_{\pv} f(\pu_\gamma,\pv_\gamma)
     \bigr \rangle \\
  &= \bigl \langle \bs{d}_{\pu}, \pu' + \gamma \bs{d}_{\pu} - \sru
     \bigr \rangle + \bigl \langle \bs{d}_{\pv}, -\srv \bigr \rangle \\
  &= \gamma \norm{\bs{d}_{\pu}}_2^2 + \pu'^\tr\bs{d}_{\pu}-\sr^\tr\bs{d}
\end{aligned}
\]
We may therefore compute the optimal step size $\gamma$ as
\begin{equation}
\label{eqn:linesearch}
    \gamma = \max\left(0, \min\left(\gamma_{\max}, 
        \frac{\sr^\tr \bs{d} - \pu'^\tr \bs{d}_{\pu}}{\norm{\bs{d}_{\pu}}_2^2}
    \right ) \right )
\end{equation}

\begin{algorithm}
\caption{Conditional gradient for {\smap}}
\begin{algorithmic}[1]
    \STATE 
        Initialize:~
        $\yy^{(0)} \leftarrow \map_\AA(\sru, \srv);
        ~\supp^{(0)}=\{\yy^{(0)}\};
        ~\p^{(0)}=\bs{e}_{\yy^{(0)}};
        ~[\pu^{(0)},\pv^{(0)}]=\col{A}{\yy^{(0)}}$

    \FOR{$t=0\dots t_{\max}$}

        \STATE $\yy\leftarrow\map_\AA(\sru-\pu^{(t)},\srv)$;
        \hskip7.7em
        $\bs{d}^{\text{F}} \leftarrow \col{\AA}{\yy}-[\pu^{(t)},\pv^{(t)}]$
        \quad(forward)

        \STATE $w \leftarrow \displaystyle\argmax_{w\in\supp^{(t)}}~
        (\sru - \pu^{(t)})^\tr \col{M}{w} + \srv^\tr \col{N}{w};
        \qquad \bs{d}^\text{W} \leftarrow
            [\pu^{(t)}, \pv^{(t)}] - \col{A}{w}$
        \quad(away)

        \IF {$\operatorname{gap}(\bs{d}^\text{F}, \pu^{(t)}) < \epsilon$}
        \STATE{{\bf return} $\pu^{(t)}$} \quad(Equation~\ref{eqn:gap-app})
        \ENDIF

        \IF {variant $=$ vanilla}
        \STATE $\bs{d} \leftarrow \bs{d}^\text{F}; \qquad \gamma_{\max}\leftarrow 1$
        \ELSIF{variant $=$ pairwise}
        \STATE $\bs{d} \leftarrow \bs{d}^\text{F}+\bs{d}^\text{W}; \qquad
        \gamma_{\max}\leftarrow y_w$
        \ELSIF{variant $=$ away-step}

        \IF{$
            \operatorname{gap}(\bs{d}^\text{F}, \pu^{(t)}) \geq
            \operatorname{gap}(\bs{d}^\text{W}, \pu^{(t)})$}
        
            \STATE $\bs{d} \leftarrow \bs{d}^\text{F}; \qquad \gamma_{\max}\leftarrow 1$
        \ELSE
            \STATE $\bs{d} \leftarrow \bs{d}^\text{A}; \qquad
            \gamma_{\max}\leftarrow {\pp_w}/{(1 - \pp_w)}$
        \ENDIF

        \ENDIF

        \STATE Compute step size $\gamma$
        \quad(Equation~\ref{eqn:linesearch})
        \STATE $[\pu^{(t+1)}, \pv^{(t+1)}] \leftarrow [\pu^{(t)},
            \pv^{(t)}] + \bs{d}$ 
        \STATE Update $\supp^{(t+1)}$ and $\p^{(t+1)}$ accordingly.
    \ENDFOR
\end{algorithmic}
\end{algorithm}

