\chapter{Conclusions}\label{chapter:conclusions}

To wrap up, in this chapter, we review the results and techniques that we
developed, and discuss their consequences, impact, and directions for future
work.

\section{Summary and Discussion}
Over the course of this work, we took an in-depth look at the possible uses of
structure in machine learning models for NLP.
Our experimental results consistently confirm that structure plays a crucial
role in natural language tasks, as structure-aware models consistently
outperform unstructured variants across many tasks.


The significance of our technical contributions lies across two fronts. On one
side, many of our constructions are generalizations or extensions of significant
pieces of the machine learning toolkit. Our sparse and structured attention
mechanisms (\chapref{sparseattn}) form a general family which
includes softmax and sparsemax, and our Fenchel-Young losses provide a similarly
extensible family of loss functions, which includes well-known losses such as
the logistic, hinge, CRF, and structured SVM losses. Generalizations of this
form are useful for shedding new light and new context onto existing concepts,
leading to practical new tools as well as to new insights: for instance,
generalizing softmax and sparsemax allowed us to rigorously capture why the
former is always dense and the latter tends to be sparse.

On the other front, our work pushes the boundary of what transformations can be
used as neural hidden layers, when training with backpropagation. By using
\emph{smoothing}, we develop differentiable transformations based on discrete
and sparsity-inducing optimization problems, standing in contrast with the
typical deep learning tradition of purely continuous and dense transformations.

A common theme in our work is sparsity, which allows us to develop tractable and
interpretable methods for latent structure, be it through structured sparsity
(\chapref{sparseattn}) or through graphical model inference
(\chapref{sparsemap}).
We thus follow in the footsteps of recent work exploring more and more the use
of structure in neural network outputs~\citep{senna,segmentalrnn} and in
attention mechanisms~\citep{structured_attn,lapata}.
By making use of sparsity, we are able empower machine learning models with new
levels of performance, expressiveness, ease of use, and interpretability.

\section{Future Directions}

\paragraph{Approximate differentiable inference in overlapping factor graphs.}
We have shown that
\smap provides efficient, sparse, and differentiable inference in any
structured model, as long as we have access to a MAP oracle. As seen in
\chapref{marseille}, there are situations in which domain-specific
structure can only be expressed through overlapping factors, necessarily making
exact MAP and marginal inference intractable. It remains to be seen 
what improvements an approximate \smap loss can lead to for structured output
models. Deriving an efficient backward pass for latent structure with
overlapping factors constitutes a challenge. 

\paragraph{User-friendly methods for defining structured sparsity.}
For combinatorial structure, \smap provides an efficient forward and backward
pass defined only in terms of a user-specified MAP inference procedure.
Such a user-friendly way of defining new structured sparsity mappings is still
elusive: proposing new transformations like fusedmax and oscarmax requires 
case-by-case study. Recent developments in atomic norms for structured sparsity
\citep{atomic} may lead to useful generalized formalisms, allowing for similar
user-friendliness as we are able to achieve for \smap.

\paragraph{Properties of learning with {\boldmath \smap} losses.}
The empirical results in Chapter~\ref{chapter:losses} suggest good convergence
and margin-like generalization properties. In the unstructured setting, the
sparsemax loss~\citep{sparsemax} enjoys a margin property. As such, further
study of the structured setting of Fenchel-Young losses, as well as the study of
specialized learning algorithms for such losses, is an interesting direction
for future research.

\paragraph{Efficient algorithms for structured sparsity.}
The projection onto the probability simplex shows up as a fundamental building
block throughout our methods for latent structured sparsity in
Chapter~\ref{chapter:sparseattn}. For fast neural network prediction and
training, developing an efficient GPU-parallelized
algorithm for projecting onto the simplex, as well as for similar projections
and optimization problems, has the potential to lead to important speedups.

\paragraph{Gradient-free learning of latent structure.} In this work, we have
focused on the most common way of training neural networks:
stochastic gradient learning. An alternative that has not been explored very
much in the literature is based on lagrangian dynamics \citep{carreira,taylor2016},
replacing gradient computation with other types of subproblems. It remains to be
seen whether such methods are competitive for latent structure applications.
