import numpy as np
from scipy import optimize

import matplotlib.pyplot as plt
from matplotlib.path import Path
from matplotlib import patches

from plot_balls import plot_norm_ball, make_lp

def plot_ball(ax):
    colors = plt.cm.Pastel1.colors
    plt.axvline(0, ls="--", color='gray', lw=1)
    plt.axhline(0, ls="--", color='gray', lw=1)

    path = plot_norm_ball(make_lp(1))
    fill = patches.PathPatch(path, facecolor=colors[4], edgecolor='k',
                            alpha=1, linewidth=1.5)

    ax.add_patch(fill)


def plot_func(a, b):
    plt.arrow(0, 0, a, b, color='gray', length_includes_head=True, lw=1.5,
              head_width=0.2)


if __name__ == '__main__':

    t = 2

    #a = 1
    #b = -1.5

    a = 2 / 3
    b = -1

    # linear
    plt.figure(figsize=(3, 3))
    ax = plt.gca()
    plot_ball(ax)
    plot_func(a, b)

    T = np.linspace(-t, t, 100)
    X, Y = np.meshgrid(T, T)

    # linear function with coefs a, b
    Z = -4 * (a * X + b * Y)
    contours = plt.contour(X, Y, Z, cmap=plt.cm.plasma)
    plt.clabel(contours, contours.levels, inline=True)

    plt.xlim(-t, t)
    plt.ylim(-t, t)
    plt.yticks(np.arange(-t, t + 0.1, 1))
    #  plt.subplots_adjust(left=0.16, right=0.94)
    plt.savefig("fig/0204_l1_sparsity_linear.pdf")

    # linear
    plt.figure(figsize=(3, 3))
    ax = plt.gca()
    plot_ball(ax)
    plot_func(a, b)

    T = np.linspace(-t, t, 100)
    X, Y = np.meshgrid(T, T)

    # linear function with coefs a, b

    s = 4
    aa = s * a
    bb = s * b

    Z = np.sqrt((X - aa) ** 2 + (Y - bb) ** 2) / s
    contours = plt.contour(X, Y, Z, cmap=plt.cm.plasma)
    plt.clabel(contours, contours.levels, inline=True)

    plt.xlim(-t, t)
    plt.ylim(-t, t)
    plt.yticks(np.arange(-t, t + 0.1, 1))
    plt.savefig("fig/0205_l1_sparsity_quad.pdf")

