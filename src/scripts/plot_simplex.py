import numpy as np
import matplotlib.pyplot as plt

import simplex
import projection

# plot linear over simplex

theta = np.array([0.5, 1, 0])
plt.figure(figsize=(4.5, 4))
proj = projection.Linear(theta)
cs = simplex.draw(proj.val, nlevels=10)
plt.clabel(cs,
           # cs.levels[1::3],
           rightside_up=True,
           inline=True,
           manual=[(.46, .6), (.4, .4), (.6, .18), (.75, .09)])
           #  manual=[(.5, t) for t in np.arange(0, 1, 0.2)])
opt = proj.project(verbose=True)
simplex.scatter(opt, marker='x', color='k')
plt.axis('off')
plt.savefig("fig/0302_linear_simplex.pdf", bbox_inches="tight")


for k, theta in enumerate([np.zeros(3),
                           np.array([.8, 1, 0])]):
    for proj in (
                 projection.Entropy(theta),
                 projection.Squared(theta),
                 #  projection.PNormMax,
                 #  projection.TsallisMax,
                 projection.Oscarmax(theta, alpha=.2),
                 projection.Fusedmax(theta, alpha=.2)
                 ):

        plt.figure(figsize=(4.5, 4))
        # plt.title("{} at {}".format(proj.title, theta))
        cs = simplex.draw(proj.val, nlevels=10)
        plt.clabel(cs,
                   # cs.levels[1::3],
                   rightside_up=True,
                   inline=True,
                   manual=[(.46, .6), (.4, .4), (.6, .18), (.75, .09)])
        opt = proj.project(verbose=True)
        print(opt)
        simplex.scatter(opt, marker='x', color='k')
        #  plt.show()
        plt.savefig("fig/0306_simplex_{}_{}.pdf".format(k, proj.title),
                    bbox_inches="tight")
