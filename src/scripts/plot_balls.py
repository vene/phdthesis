import numpy as np
from scipy import optimize

import matplotlib.pyplot as plt
from matplotlib.path import Path
from matplotlib import patches


def plot_norm_ball(f):
    pos = []
    for x in np.linspace(0, 1, 200):
        # find y st Omega([x, y]) = 1
        obj = lambda y: f(np.array([x, y])) - 1
        y = optimize.brentq(obj, 0, 1)

        pos.append((x, y))

    rest = []
    for x, y in pos[::-1]:
        rest.append((x, -y))

    for x, y in pos:
        rest.append((-x, -y))

    for x, y in pos[::-1]:
        rest.append((-x, y))

    points = pos + rest + [pos[0]]
    codes = [Path.LINETO] * len(points)
    codes[0] = Path.MOVETO
    codes[-1] = Path.CLOSEPOLY

    path = Path(points, codes)
    return path


def make_lp(p):
    def lp(x):
        return np.sum(np.abs(x) ** p) ** (1 / p)
    return lp

if __name__ == '__main__':

    plt.figure(figsize=(4.5, 3))
    ax = plt.gca()

    colors = plt.cm.Pastel1.colors


    plt.axvline(0, ls="--", color='gray', lw=1)
    plt.axhline(0, ls="--", color='gray', lw=1)

    for k, p in enumerate([1e10, 3,  2, 1.5, 1, 0.5]):

        path = plot_norm_ball(make_lp(p))
        fill = patches.PathPatch(path, facecolor=colors[k], edgecolor='k',
                                 alpha=1, linewidth=1.5,
                                 label="$p={}$".format(
                                     (r'\infty' if p > 10 else p)))
        ax.add_patch(fill)

        #  outline = patches.PathPatch(path, fill=False, linewidth=2, color='k')
        #  ax.add_patch(outline)

    t = 1.1
    plt.xlim(-t, t)
    plt.ylim(-t, t)
    plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0)
    plt.tight_layout()
    plt.subplots_adjust(right=0.7)
    #  plt.show()
    plt.savefig("fig/0202_lp_balls.pdf")

