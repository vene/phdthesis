from __future__ import division

import numpy as np

from fista import fista
from projection import project_simplex
from projection import prox_oscar
from lightning.impl.penalty import prox_tv1d


def soft_argmax(x, gamma=1.0):
    x = x / gamma
    max_x = np.max(x)
    exp_x = np.exp(x - max_x)
    return exp_x / exp_x.sum()


def hard_argmax(x):
    j = np.argmax(x)
    e = np.zeros(len(x))
    e[j] = 1
    return e


def sparse_max(x, gamma=1.0):
    x = np.array(x)
    y = project_simplex(x / gamma)
    return np.dot(x, y) - 0.5 * gamma * np.dot(y, y)


def sparse_argmax(x, gamma=1.0):
    x = np.array(x)
    return project_simplex(x / gamma)


def ovo(x, func):
    ret = np.zeros_like(x)
    for i in range(len(x)):
        prod = 1
        for j in range(len(x)):
            if i == j:
                continue
            prod *= func(x[i] - x[j])
        ret[i] = prod
    ret /= np.sum(ret)
    return ret


def ovo_smooth_ramp(x, gamma=1.0):
    func = lambda u: smooth_ramp(u / gamma)
    return ovo(x, func)


def logistic(t, gamma=1.0):
    return 1. / (1. + np.exp(-t / gamma))


def ovr_logistic(x, gamma=1.0):
    return np.array([logistic(val, gamma) for val in x])


def smooth_ramp(x):
    # http://math.stackexchange.com/questions/101480/are-there-other-kinds-of-bump-functions-than-e-frac1x2-1
    if x <= -1:
        return 0
    if x >= 1:
        return 1

    return 1 / (1 + np.exp(4 * x / (x ** 2 - 1)))


def pnorm(y, p):
    return np.sum(np.abs(y) ** p) ** (1. / p)
    # Assume y >= 0
    #return np.sum(y ** p) ** (1. / p)


def pnorm_grad(y, p):
    g = 1. / p * np.sum(np.abs(y) ** p) ** (1./ p - 1)
    g *= p * (np.abs(y) ** (p-1)) * np.sign(y)
    #g = 1. / p * np.sum(y ** p) ** (1./ p - 1)
    #g *= p * (y ** (p-1))
    return g

def grid_search(x, func, gamma=1.0, verbose=0):
    # Brute force grid-search. Only for the binary case.

    x = np.array([x, 0])
    best = -np.inf
    for y1 in np.linspace(0, 1, 1000):
        y = np.array([y1, 1 - y1])
        val = np.dot(x, y) - gamma * func(y)
        if val > best:
            best = val

    if verbose:
        print("best", best)

    return best


def pnorm_max_argmax(x, p=1.5, gamma=1.0, squared=True, max_iter=20,
                     tol=1e-3, verbose=0):

    x = np.array(x)

    def sfunc(y, args=None, ret_grad=False):
        if squared:
            obj = np.dot(x, y) - 0.5 * gamma * pnorm(y, p) ** 2
        else:
            obj = np.dot(x, y) - gamma * pnorm(y, p) ** (1 + int(squared))

        #obj = np.dot(x, y) - gamma * 0.5 * np.dot(y, y)

        if not ret_grad:
            # Convert to a minimization problem.
            return -obj

        if squared:
            grad = x - gamma * pnorm(y, p) * pnorm_grad(y, p)
        else:
            grad = x - gamma * pnorm_grad(y, p)
        #grad = x - gamma * y

        return -obj, -grad

    def nsfunc(y, L):
        y = project_simplex(y)
        return y, 0

    y0 = np.ones_like(x)
    y0 /= len(y0)
    y = fista(sfunc, nsfunc, y0, verbose=verbose, max_iter=max_iter, tol=tol)

    return -sfunc(y, ret_grad=False), y


def pnorm_max(*args, **kw):
    return pnorm_max_argmax(*args, **kw)[0]


def pnorm_argmax(*args, **kw):
    return pnorm_max_argmax(*args, **kw)[1]


@np.vectorize
def huber(x, mu=1):
    if np.abs(x) <= mu:
        return x ** 2 / (2 * mu)
    else:
        return np.abs(x) - mu / 2

def _huber_p_one(x):
    if np.abs(x) <= 1:
        return x
    elif x > 1:
        return 1
    else:
        return -1

def huber_p(x, mu=1):
    out = x.copy()
    out /= mu
    out[x > mu] = 1
    out[x < -mu] = -1
    return out


def hused_lasso_obj_grad(x, mu=1.0, ret_grad=False):
    consecutive = x[1:] - x[:-1]
    obj = np.sum(huber(consecutive, mu))

    if ret_grad:
        grad = np.zeros_like(x)
        h_grad = huber_p(consecutive, mu)
        grad[:-1] -= h_grad
        grad[1:] += h_grad

        return obj, grad

    return obj


def hused_max_argmax(x, gamma=1.0, mu=1.0, max_iter=20,
                     verbose=0):

    # hused = huber-fused

    x = np.array(x)

    def sfunc(y, args=None, ret_grad=False):

        hused = hused_lasso_obj_grad(y, mu, ret_grad)
        obj = np.dot(x, y) - 0.5 * np.dot(y, y)
        if ret_grad:
            hused, hused_p = hused
            obj -= gamma * hused
            grad = x - gamma * hused_p  - y
            return -obj, -grad

        else:
            obj -= gamma * hused
            return -obj

    def nsfunc(y, L):
        y = project_simplex(y)
        return y, 0

    y0 = np.ones_like(x)
    y0 /= len(y0)
    y = fista(sfunc, nsfunc, y0, verbose=verbose, max_iter=max_iter)

    return -sfunc(y, ret_grad=False), y


def hused_max(*args, **kw):
    return hused_max_argmax(*args, **kw)[0]


def hused_argmax(*args, **kw):
    return hused_max_argmax(*args, **kw)[1]


def fused_max_argmax(x, alpha=1.0):
    tmp = x.copy()
    prox_tv1d(tmp, alpha)
    y = project_simplex(tmp)
    # todo check obj value
    #obj = 0.5 * np.linalg.norm(x - y)
    #obj += alpha * np.abs(np.diff(y)).sum()
    obj = np.dot(x, y)
    obj -= 0.5 * np.dot(y, y)
    obj -= alpha * np.abs(np.diff(y)).sum()
    return obj, y


def fused_max(*args, **kw):
    return fused_max_argmax(*args, **kw)[0]


def fused_argmax(*args, **kw):
    return fused_max_argmax(*args, **kw)[1]


def oscar_max_argmax(x, alpha=1.0):
    y = project_simplex(prox_oscar(x, alpha))
    obj = np.dot(x, y)
    obj -= 0.5 * np.dot(y, y)
    for j in range(len(x)):
        for j2 in range(j+1, len(x)):
            obj -= alpha * max(np.abs(y[j]), np.abs(y[j2]))
    return obj, y


def oscar_max(*args, **kw):
    return oscar_max_argmax(*args, **kw)[0]


def oscar_argmax(*args, **kw):
    return oscar_max_argmax(*args, **kw)[1]
