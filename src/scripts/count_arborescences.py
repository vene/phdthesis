# count possible dependency trees via the matrix-tree theorem

import numpy as np

def count_arborescenes_matrix_tree(n):
    A = np.ones((n, n))
    A[np.diag_indices_from(A)] = 0
    deg = A.sum(axis=0)
    L = -A + np.diag(1 + deg)
    return np.linalg.det(L)

def count_arborescenes_closed_form(n):
    return (n + 1) ** (n - 1)

for i in range(15):
    a = count_arborescenes_matrix_tree(i)
    b = count_arborescenes_closed_form(i)
    print(a - b, b)

