import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
from mpl_toolkits.axes_grid1.inset_locator import (
    zoomed_inset_axes, mark_inset)
from projection import project_simplex
from max_operators import pnorm_max_argmax

from lightning.impl.penalty import prox_tv1d


@np.vectorize
def max_w0(x):
    return x if x > 0 else 0.0

@np.vectorize
def softmax_w0(x):
    return np.log1p(np.exp(x))

@np.vectorize
def sparsemax_w0(x):
    x = np.array([x, 0.0])
    y = project_simplex(x)
    return np.dot(y, x) - 0.5 * np.dot(y, y)

@np.vectorize
def fusedmax_w0(x, alpha=0.25):
    x = np.array([x, 0.0])
    y = x.copy()
    prox_tv1d(y, alpha)
    y = project_simplex(y)
    return np.dot(y, x) - (0.5 * np.dot(y, y) + alpha * np.sum(np.abs(np.diff(y))))

@np.vectorize
def pnormax_w0(x):
    x = np.array([x, 0.0])
    _, y = pnorm_max_argmax(x, max_iter=1000, tol=1e-8)
    return np.dot(y, x) - (0.5 * np.linalg.norm(y, 1.5) ** 2)

@np.vectorize
def argmax1_w0(x):
    return 1 if x > 0 else 0

@np.vectorize
def softargmax1_w0(x):
    return np.exp(x) / (1 + np.exp(x))

@np.vectorize
def sparseargmax1_w0(x):
    x = np.array([x, 0.0])
    y = project_simplex(x)
    return y[0]

@np.vectorize
def fusedargmax1_w0(x, alpha=0.25):
    x = np.array([x, 0.0])
    y = x.copy()
    prox_tv1d(y, alpha)
    y = project_simplex(y)
    return y[0]

@np.vectorize
def argpnormax1_w0(x):
    x = np.array([x, 0.0])
    _, y = pnorm_max_argmax(x, max_iter=1000, tol=1e-8)
    return y[0]

if __name__ == '__main__':
    xx = np.linspace(-4, 4, 500)

    y_max = max_w0(xx)
    y_soft = softmax_w0(xx)
    y_sparse = sparsemax_w0(xx)
    y_fused = fusedmax_w0(xx)
    y_pnormax = pnormax_w0(xx)

    y_argmax = argmax1_w0(xx)
    y_argsoft = softargmax1_w0(xx)
    y_argsparse = sparseargmax1_w0(xx)
    y_argfused = fusedargmax1_w0(xx)
    y_argpnormax = argpnormax1_w0(xx)

    plt.figure(figsize=(15, 5))

    #  mpl.rc('mathtext', fontset="stix", rm="serif")
    # mpl.rc('text', usetex=True)
    # mpl.rc('text.latex', unicode=True)

    # mpl.rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
    # matplotlib.rcParams['text.usetex'] = True
    # matplotlib.rcParams['text.latex.unicode'] = True
    mpl.rc('lines', linewidth=3.5)
    mpl.rc('font', size=22)

    #  colormap = plt.cm.Dark2.colors
    colormap = plt.cm.tab10.colors
    colors = dict(
        softmax=colormap[0],
        sparsemax=colormap[1],
        fusedmax=colormap[3],
        pnorm=colormap[4],
    )

    plt.subplot(121)
    plt.plot(xx, y_max - np.min(y_max), label="max", color='black', alpha=0.5,
             zorder=0)
    plt.plot(xx, y_soft - np.min(y_soft), label="softmax", ls=":", zorder=4,
             color=colors['softmax'])
    plt.plot(xx, y_sparse - np.min(y_sparse), label="sparsemax", ls="--",
             color=colors['sparsemax'],
             zorder=3)
    plt.plot(xx, y_pnormax - np.min(y_pnormax), label="sq-pnorm-max", zorder=2,
             color=colors['pnorm'])
    plt.plot(xx, y_fused - np.min(y_fused), label="fusedmax", zorder=5,
             color=colors['fusedmax'],
             linewidth=6)
    plt.title(r"max$_\Omega([t, 0]) + $ const")
    plt.xlabel('$t$')
    plt.legend()

    plt.subplot(122)
    plt.plot(xx[xx < 0], y_argmax[xx < 0], color='black', alpha=0.5, zorder=0)
    plt.plot(xx[xx > 0], y_argmax[xx > 0], color='black', alpha=0.5, zorder=0)
    # plt.axhline(
    plt.plot(xx, y_argsoft, ls=":", zorder=4, color=colors['softmax'])
    plt.plot(xx, y_argsparse, ls="--", zorder=3, color=colors['sparsemax'])
    plt.plot(xx, y_argpnormax, zorder=2, color=colors['pnorm'])
    plt.plot(xx, y_argfused, zorder=5, linewidth=6, color=colors['fusedmax'])
    plt.title(r"$\Pi_\Omega([t, 0])_1 = \nabla$max$_\Omega([t, 0])_1$")
    plt.xlabel('$t$')
    ax = plt.gca()
    axins = zoomed_inset_axes(ax, 12, loc=4)
    axins.plot(xx, y_argsoft, ls=":", zorder=4, color=colors['softmax'])
    axins.plot(xx, y_argsparse, ls="--", zorder=3, color=colors['sparsemax'])
    axins.plot(xx, y_argpnormax, zorder=2, color=colors['pnorm'])
    axins.plot(xx, y_argfused, zorder=1, color=colors['fusedmax'])
    axins.set_xlim(-1.1, -0.9)
    axins.set_ylim(-0.02, 0.02)
    plt.yticks(visible=False)
    plt.xticks(visible=False)
    mark_inset(ax, axins, loc1=2, loc2=3, fc="none", ec="0.7", lw=2)

    plt.subplots_adjust(left=0.04, bottom=0.155, right=0.99, top=0.92,
                        wspace=0.18, hspace=0.2)
    plt.savefig("fig/0304_max_argmax.pdf")
    #  plt.show()
