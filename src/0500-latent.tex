\chapter{Dynamically Inferred Neural Network Structure}\label{chapter:latent}

In the previous chapter, we introduced {\smap}, a differentiable sparse
inference strategy allowing hidden layers with structured outputs. The output
of such a hidden layer, however, is just a vector encoding of the selected
structures.  We now extend this idea one step further, by permitting the latent
structure to define arbitrary structure-dependent computation.

A host of neural architectures for NLP tasks are designed to follow some
underlying structural representation of the data itself; for instance,
tree-structured recursive neural networks compose words according to the
syntactic relationships between them, typically using off-the-shelf parsers.
Recent attempts to jointly learn the latent structure encounter a trade-off: make
factorization assumptions that limit expressiveness, or sacrifice end-to-end
differentiability.
Using the recently proposed \smap inference, which retrieves a sparse
distribution over latent structures, we propose a novel approach for end-to-end
learning of latent structure predictors jointly with a downstream predictor.
To the best of our knowledge, our method is the first to enable
unrestricted dynamic computation graph construction from the \emph{global} latent
structure, while maintaining differentiability.

%\emph{This chapter is based on \citep[under review]{latentstruct}}

\section{Overview and Related Work}
Latent structure models are a powerful tool for modeling compositional data and
building NLP pipelines \citep{noahbook}. 
An interesting emerging direction is to {\bf dynamically} adapt
a network's computation graph, based on structure inferred from the input;
notable applications include
learning to write programs \citep{diffforth},
answering visual questions by composing specialized modules \citep{modulenets,visreason},
speeding up image classification by skipping hidden layers \citep{andreas},
and composing sentence representations using latent syntactic parse trees
\citep{latentparse}.

But how to learn a model that is able to condition on such combinatorial variables? 
How to marginalize over latent structures? 
For tractability, existing approaches have to make a choice. 
Some of them eschew \emph{global}
latent structure, resorting to computation graphs built from smaller local
decisions: \eg, 
structured attention networks \citep{structured_attn,lapata}, as well as our
networks from \chapref{sparsemap}, use local posterior marginals $\pu$ as
attention weights,
\citet{andreas} make binary decisions to keep or skip each layer,
\citet{dani} construct sentence representations from parser chart entries. 
Others allow more flexibility at the cost of losing end-to-end
differentiability, ending up with reinforcement learning problems
\citep{latentparse,modulenets,visreason,isitsyntax}. 
Approaches based on continuous relaxations of discrete structures,
such as iterative neural decoders \citep{easyfirst}, have similar shortcomings. 

More traditional approaches employ an off-line structure predictor (\eg, a
parser) to define the computation graph \citep{treelstm,esim}, 
sometimes with some parameter sharing~\citep{spinn}.
However, these off-line methods are unable to {\em jointly} train the latent model and
the downstream classifier via error gradient information.

%In this work, we propose
We propose here
a new strategy for  
building \textbf{dynamic computation graphs} with latent structure,
through \textbf{sparse structure prediction}. 
Sparsity allows selecting and conditioning on a tractable number of global structures, 
eliminating the limitations stated above.  
Namely,  our approach is the first that:
\begin{enumerate}
\item[{\bf A)}] is {\bf fully differentiable};
\item[{\bf B)}] supports {\bf latent structured variables};
\item[{\bf C)}] can marginalize over full {\bf global structures}.
\end{enumerate}
This contrasts with off-line and with reinforcement learning-based approaches,
which satisfy {\bf B} and {\bf C} but not {\bf A}; and with
local marginal-based methods
such as structured attention networks,
which satisfy {\bf A} and {\bf B}, but not {\bf C}.  Key to our approach is
\textbf{{\boldmath \smap} inference}, introduced in \chapref{sparsemap},
which induces, for each data example, a very sparse posterior
distribution over the possible structures, allowing us to compute
the expected network output efficiently and explicitly in terms of a small,
interpretable set of latent structures. 
Our model can be trained end-to-end with gradient-based methods, without the
need for policy exploration or sampling. 

We demonstrate our strategy on inducing latent dependency Tree\-LSTMs,
achieving competitive results on sentence classification, natural language
inference, and reverse dictionary lookup. 

\section{Models with Latent Structured Variables}
\begin{figure}
    \centering
    \includegraphics[width=0.99\textwidth]{fig/sparsemap-cg}
\caption{\label{fig:latentsketch}Our method computes a sparse probability distribution over all
possible latent structures: in this illustration, there are only three
dependency trees with nonzero probability. For each such structure $\yy$, we 
may evaluate $p_\clfp(c\,|\,x,\yy)$ by constructing the corresponding computation graph.
For conciseness, the dependence on $x$ is omitted from the figure.}
\end{figure}

We describe our proposed approach for training latent variable models, where the
latent variables are combinatorial structures, in particular non-projective
dependency parse trees.

Let $x$ denote the input, and $c$ denote an output class.
Denote by $\yy \in \trees(x)$
a latent structured variable; for example,
$\trees(x)$ denotes the set of possible dependency trees for $x$.
We would like to train a neural network to model
\begin{equation}\label{eqn:latent}
p(c\,|\,x) \coloneqq \sum_{\yy \in \trees(x)} p_\parp(\yy\,|\,x)
\,p_\clfp(c\,|\,x,\yy),
\end{equation}
\noindent where
$p_\parp(\yy\,|\,x)$ is a structured-output parsing model that defines a
distribution over trees, and $p_\clfp(c\,|\,x,\yy)$
is a classifier whose computation graph may depend \textbf{freely and globally}
on the structure $\yy$ (\eg, a TreeLSTM).
The rest of this section focuses on the challenge of defining
$p_\parp(\yy\,|\,x)$ such that \eqnref{latent} remain tractable and
differentiable.

\paragraph{Global inference.}
Denote by $\score(\yy;\,x)$ a scoring function, assigning each tree a
non-normalized score. For instance, we may have an \emph{arc-factored} score
$\score(\yy; x) \coloneqq \sum_{a \in \yy} \arcscore(a; x)$, where we 
interpret a tree $\yy$ as a set of directed arcs $a$, each receiving an atomic score 
$\arcscore(a; x)$. 
As discussed in \secref{smap-bg}, deriving $p_\parp$ given $\score$ is known as
\emph{structured inference}.
%In many existing frameworks, 
This can be written as a $\O$-regularized optimization problem of the form
\begin{equation}
\label{eqn:generalinflatent}
p_\parp(\cdot | x)
\coloneqq 
\argmax_{\bs{q} \in\Simplex^{|\trees(x)|}} \sum_{\yy \in \trees(x)}
q(\yy) \score(\yy; x) - \O(\bs{q}),
\end{equation}
where $\Simplex^{|\trees(x)|}$ is the set of all possible probability distributions over
$\trees(x)$, and $\O$ is a convex regularizer. 
Note that this time we are interested in the entire probability distribution, not just the variable
marginals, as in \chapref{sparsemap}.

\textbf{Marginal inference}, obtained by setting  $\O(\bs{q})\coloneqq\sum_{\yy \in \trees(x)} q(\yy)\log q(\yy)$,
provides a differentiable expression for $p_\parp$. However, crucially,
since $\exp(\cdot)>0$, every tree is assigned strictly nonzero probability.
Therefore---unless the downstream $p_\clfp$ is constrained to also factor
over arcs, as in \citet{structured_attn}; \citet{lapata}, or more generally
in \citet{arthurdp}---the sum in \eqnref{latent} requires
enumerating the exponentially large $\trees(x)$. This is generally intractable, and even hard to
approximate via sampling, even when $p_\parp$ is tractable.
%
At the polar opposite, setting $\O(\bs{q})\coloneqq0$ yields
\textbf{maximum a posteriori ({\boldmath \map}) inference}, which assigns a
probability of $1$ to the highest-scoring tree, and $0$ to all
others, yielding a very sparse $p_\parp$.
However, since the top-scoring tree (or top-$k$, for fixed $k$) does not vary with
small changes in $\parp$, error gradients cannot propagate through \map.
This prevents end-to-end gradient-based training for \map-based latent variables,
which makes them more difficult to use.

\paragraph{{\boldmath \smap} inference.} Our key insight is to use the sparse posterior
induced by \smap (\chapref{sparsemap}) 
to sparsify the set $\trees$ while preserving differentiability.
\smap uses a quadratic penalty on the posterior marginals
\begin{equation*}
    \O(\bs{q}) \coloneqq \norm{\bs{u}(\bs{q})}_2^2,
    ~~\text{where}~~
    \left[\bs{u}(\bs{q})\right]_a \coloneqq \sum_{h: a \in h}  q(h).
\end{equation*}
\smap assigns nonzero probability to only a small set of plausible trees
$\bar\trees \subset \trees$, of size at most equal to the number of arcs
\citep[Proposition~11]{ad3}. This guarantees that the summation in \eqnref{latent}
can be computed efficiently by iterating over $\bar\trees$, as depicted in
\figref{latentsketch}. The algorithms from \secref{smap-solve} can be used to compute
$p\parp$ in the forward pass.
\footnote{While the \smap arc posteriors $\bs{u}$ are always unique, 
for pathologic inputs (\ie, not in general position), there may be more than one
optimal distribution $p_\parp$. This did not pose any problems in practice, where any
ties would be broken at random.} 
It remains to characterize the \textbf{backward pass},
\ie, $\nicefrac{\partial p_\parp(\yy\,|\,x)}{\partial \parp}$, in order to
enable end-to-end gradient-based training of structured latent variable models.
The next proposition provides this.

\begin{proposition}\label{prop:latent-backward}
Let $p_\parp(\yy | x)$ denote the \smap posterior probability distribution,
\ie, a solution of \eqnref{generalinflatent} for $\O(\bs{q}) = \norm{\pu(\bs{q})}_2^2$,
where $u_a(\bs{q}) = \sum_{\yy: a \in \yy} q(\yy) = \sum_\yy m_{a,\yy} q(\yy)$ for an
appropriately defined indicator matrix $\bs{M}$.
Define $\bs{Z} \coloneqq \left({\bs{M}_{\bar\trees(x)}}^\tr \bs{M}_{\bar\trees(x)}\right)^{-1}
\in\real^{|\bar{\trees}|\times|\bar{\trees}|}$,
the sum of column $\yy$ of $\bs{Z}$ by $\varsigma(\yy) \coloneqq 
\sum_{\yy' \in \bar\trees(x)}
z_{\yy', \yy}$, and the overall sum of $\bs{Z}$ by $\zeta \coloneqq \sum_{\yy' \in \bar\trees(x)}
\varsigma(\yy')$.

Then, for any $\yy\in\trees(x)$, we have
\begin{equation*}
    \pfrac{p_\parp(\yy | x)}{\parp} =
    \begin{cases}
    \sum_{\yy' \in \bar\trees(x)} 
    \left( z_{\yy,\yy'} -
        % \frac{(\bs{1}^\tr \bs{z}_h) \cdot (\bs{1}^\tr \bs{z}_{h'})}{\bs{1}^\tr\bs{Z1}}
    \zeta^{-1} \varsigma(\yy) \varsigma(\yy')
    \right) \nicefrac{\partial \score(\yy';~x)}{\partial \parp}, &
    p_\parp(\yy | x) > 0 \\
    0, & p_\parp(\yy | x) = 0. \\
    \end{cases}
\end{equation*}
\end{proposition}
\textbf{Crucially, this gradient term has the same sparsity pattern
as $p_\parp$, and is efficient to compute,} amounting to multiplying by
a $|\bar\trees(x)|$-by-$|\bar\trees(x)|$ matrix. The proof
is given in Appendix~\ref{proof:latent-backward} as a slight variation of the
\smap backward pass (Proposition~\ref{prop:backw}).

\paragraph{Generality.}
Our description focuses on probabilistic
classifiers,
%with latent dependency trees,
but our method can be readily
applied to networks that output any representation, not necessarily a
class probability. For this, we define a function $\bs{r}_\clfp(x,\yy)$,
consisting of any auto-differentiable computation w.r.t.\ $x$,
conditioned on the discrete latent structure $\yy$ in arbitrary,
non-differentiable ways. We then compute % the expectation
\begin{equation*}
\bar{\bs{r}}(x) \coloneqq
\hspace{-5pt}
\sum_{\yy \in \trees(x)}
\hspace{-5pt}
p_\parp(\yy\,|\,x) \bs{r}_\clfp(x,\yy)\\
=\mathbb{E}_{\yy \sim p_\parp} \bs{r}_\clfp(x,\yy).
\end{equation*}
This strategy is demonstrated in our reverse-dictionary experiments.
%
In addition, our approach is not limited to trees:
any structured model with tractable \map inference may be used.

\section{Latent Dependency TreeLSTM}
We combine the word vectors $\bs{v}_i$ in a sentence into a single vector using
a tree-structured Child-Sum LSTM, which allows an arbitrary number of children
at any node \citep{treelstm}.  Our baselines consist in extreme cases of
dependency trees: where the parent of word $i$ is word $i+1$ (resulting in a
{\bf left-to-right} sequential LSTM), and where all words are direct children
of the root node (resulting in a {\bf flat} additive model). We also consider
{\bf off-line} dependency trees precomputed by Stanford CoreNLP \citep{corenlp}.

\paragraph{Neural arc-factored dependency parsing.}
To compute the arc score $\arcscore(a; x)$
% for an arc  $a=(i \rightarrow j)$,
we follow \citet{kg} in using a multi-layer perceptron with one hidden layer.

\section{Experiments}

\paragraph{Setup and overview.}
We tackle three natural language processing tasks: sentence-level
classification (on sentiment and subjectivity datasets), natural language
inference, and reverse dictionary lookup. In all cases, we use the common
abstraction of a \emph{sentence encoder}: a module that composes a sentence
into a single vector.
%
All networks are trained via the stochastic gradient method with 16 samples per batch.
We tune the learning rate on the validation
set on a logarithmic grid. We decay the learning rate by a factor of $0.9$
after every epoch at which the validation performance is not the best seen,
and stop after five epochs without improvement.
At test time, we scale the arc scores $\arcscore$ by
a temperature $t$ chosen on the validation set, controlling the
sparsity of the \smap distribution.
All hidden layers are $300$-dimensional.

\paragraph{Sentence classification.}
We evaluate our models for sentence-level subjectivity classification
\citep{subj} and for binary sentiment classification on the Stanford
Sentiment Treebank \citep{sst}.
In both cases, we use a softmax output layer on top of the Dependency TreeLSTM
output representation.

\paragraph{Natural language inference.}
We apply our strategy to the SNLI corpus~\citep{snli}, which
consists of classifying premise-hypothesis sentence pairs into entailment,
contradiction or neutral relations. In this case, for each pair ($x_P, x_H$),
the running sum is over {\em two} latent distributions over parse trees:
\[
\sum_{\substack{\yy_P \in \trees(x_P)\\
                \yy_H \in \trees(x_H)}}
\hspace{-0.3cm}
p_\clfp(c\,|\, x_{\{P,H\}}, \yy_{\{P,H\}}) \\
~p_\parp(\yy_P|x_P)
~p_\parp(\yy_H|x_H).
\]
For each pair of trees, we independently encode the premise and hypothesis
using a dependency TreeLSTM. To obtain a pair encoding, we
concatenate the two vectors, their difference, and their element-wise product
\citep{mou}.
The result is passed through one {\em tanh} hidden layer, followed by the
softmax output layer.%
% \footnote{For NLI, our architecture is motivated by our goal of evaluating
% the impact of latent structure for learning compositional sentence
% representations. State-of-the-art models conditionally transform the sentences
% to achieve better performance, \eg, 88.6\% accuracy in \citet{esim}.}

\paragraph{Reverse dictionary lookup.}
%\label{sec:revdict}
The reverse dictionary task aims to compose the words in a dictionary
definition into an embedding that is close to the defined word.
We therefore used {\em fixed} input and output embeddings, set to unit-norm
500-dimensional vectors provided, together with training and evaluation data,
by \citet{hill}. 
For this task, the output is a computed by projecting the TreeLSTM encoding
back to the dimension of the word embeddings, and normalizing the result to 
have unit $\ell_2$ norm. The training objective is to maximize the cosine
similarity of the predicted embedding with the word being defined. The
evaluation is grouped into seen definitions, unseen definitions, and
conceptual definitions.

\paragraph{Baselines.} We compare our latent TreeLSTM to
fixed-dependency baselines:
\begin{itemize}
\item \textbf{left-to-right}: every sentence is composed sequentially from left
to right: this is essentially a standard LSTM recurrent encoder; 

\item \textbf{flat}: sentences are considered flat trees, where all words are
directly connected to the root.  This ignores all order information and is
similar to an \emph{deep averaging network}, except for the LSTM-style gated
composition rule;

\item \textbf{off-line}: we obtain predicted dependency trees from Stanford
CoreNLP.
\end{itemize}
\begin{table}
\centering
\tlfstyle
\begin{tabular}{r r r r r }
\toprule
                  & subj.   &  SST        &      SNLI  \\
\midrule
left-to-right & \textbf{92.71} &      82.10  &      80.98 \\
flat          &     92.56   &      83.96  &      81.74 \\
off-line      &     92.15   &      83.25  &      81.37 \\
latent        &     92.25   & \textbf{84.73} & \textbf{81.87} \\
\bottomrule
\end{tabular}
\caption{\label{tab:clf}Test accuracy for classification and natural language
inference.}
\end{table}

\setlength{\tabcolsep}{3.3pt}
\begin{table}[t]
\tlfstyle
\centering
\begin{tabular}{r@{\quad}c c c@{\quad}c c c@{\quad}c c c}
\toprule
          & \multicolumn{3}{c}{seen}
          & \multicolumn{3}{c}{unseen}
          & \multicolumn{3}{c}{concepts} \\
          & rank & acc$^{10}$ & acc$^{100}$
          & rank & acc$^{10}$ & acc$^{100}$
          & rank & acc$^{10}$ & acc$^{100}$ \\
\midrule
left-to-right   &      17  &      42.6  &      73.8 &         43 &      33.2  &      61.8  &        28   &     35.9  &     66.7 \\ 
flat            &      18  &      45.1  &      71.1 &   \textbf{31} & \textbf{38.2} & \textbf{65.6} &        29   &     34.3  &     68.2 \\ 
latent          & \textbf{12} & \textbf{47.5} & \textbf{74.6} &        40 &      35.6  &      60.1  &   \textbf{20}  &\textbf{38.4} &\textbf{70.7} \\ 
\midrule
{\osfstyle \citet{dani}} & 58 & 30.9 & 56.1 & 40 & 33.4 & 57.1 & 40 & 57.1 & 62.6 \\
{\osfstyle \citet{hill}} & 12 & 48 & 28 & 22 & 41 & 70 & 69& 28 & 54 \\
\bottomrule
\end{tabular}
\caption{\label{tab:revdict}Results on the reverse dictionary lookup task
\citep{hill}. Following the authors, for an input definition, we rank a
shortlist of approximately 50k candidate words according to the cosine
similarity to the output vector, and report median rank of the expected word,
accuracy at 10, and at 100.}
\end{table}
\setlength{\tabcolsep}{6pt}

\begin{figure}
\centering
{\small 28\%} \\
\begin{dependency}[hide label,edge unit distance=1.1ex]
\begin{deptext}[column sep=0.4cm]
$\star$ \& a \& vivid \& cinematic \& portrait \& .\\
\end{deptext}
\depedge{1}{2}{1.0}
\depedge{1}{3}{1.0}
\depedge{1}{4}{1.0}
\depedge{1}{5}{1.0}
\depedge{1}{6}{1.0}
\end{dependency}
\\[1em]{\small \checkmark~16\%}\\
\begin{dependency}[hide label,edge unit distance=1.1ex]
\begin{deptext}[column sep=0.4cm]
$\star$ \& a \& vivid \& cinematic \& portrait \& .\\
\end{deptext}
\depedge{5}{2}{1.0}
\depedge{5}{3}{1.0}
\depedge{5}{4}{1.0}
\depedge{1}{5}{1.0}
\depedge{5}{6}{1.0}
\end{dependency}
\\[1em]{\small $\cdots$\\13\%}\\
\begin{dependency}[hide label,edge unit distance=1.1ex]
\begin{deptext}[column sep=0.4cm]
$\star$ \& a \& vivid \& cinematic \& portrait \& .\\
\end{deptext}
\depedge{4}{2}{1.0}
\depedge{4}{3}{1.0}
\depedge{6}{4}{1.0}
\depedge{4}{5}{1.0}
\depedge[edge unit distance=0.8ex]{1}{6}{1.0}
% \depedge[edge unit distance=1ex]{6}{2}{1.0}
% \depedge[edge unit distance=1ex]{6}{3}{1.0}
% \depedge[edge unit distance=1.66ex]{1}{4}{1.0}
% \depedge{6}{5}{1.0}
% \depedge{4}{6}{1.0}
\end{dependency}
\\[-0.3cm]{$\small \cdots$}
\caption{\label{fig:trees}%
Three of the sixteen trees with nonzero probability for an SST test
example.
Flat trees, such as the first one,
perform well on this task,
as reflected by the baselines.
The second tree, marked with \checkmark, agrees with the off-line parser.}
\end{figure} 

\paragraph{Results.}
Classification and NLI results are reported in Table~\ref{tab:clf}.
Compared to the latent structure model of \citet{latentparse},
our model performs better on SNLI (80.5\%) but worse on SST (86.5\%).
On SNLI, our model also outperforms \citet{dani} (81.6\%). To our knowledge,
latent structure models have not been tested on subjectivity classification.
Surprisingly, the simple flat and left-to-right baselines are very strong,
outperforming the off-line dependency tree models on all three datasets.
The latent dependency tree model reaches the best accuracy on two out of the
three datasets. Its performance is, in part, due to its adaptability:
Figure~\ref{fig:trees} shows that, on sentiment classification, where the
flat baseline is strong, the latent model prefers flat parses, while also
assigning high scores to meaningful deeper ones.
On reverse dictionary lookup (Table~\ref{tab:revdict}), our latent model also
performs well, most noticeably on the concept classification task, where the
input definitions are more different from the ones seen during training.
For comparison, we repeat here the CKY-based latent TreeLSTM model of
\citet{dani}, as well as the LSTM-based model of \citet{hill};
as these results are from different-sized models, they are not entirely
comparable.

\section{Chapter Summary}
We presented a novel approach for training latent structure neural models,
based on the key idea of sparsifying the set of possible structures. We used
the proposed method to train competitive models based on latent dependency
TreeLSTMs. The flexibility of our method opens up
several avenues for future work.

On one side, our method can be used with any structure for which \map inference
is available (\eg, matchings, alignments).  On another, since we have no
restrictions on the way in which $p_{\bs{\xi}}(y, h)$ may depend on the latent
structure $h$, we may design latent versions of more complicated state-of-the-art
models, such as ESIM for natural language inference~\citep{esim}.
%.
Partial or distant supervision \citep{latentparse}, can be
applied to the latent parsers in our models for little computational cost,
using the \smap loss introduced in the next chapter.
