\chapter{Background}\label{chapter:bg}
This chapter serves as a reminder of the topics our work builds upon:
machine learning, neural network architecture, convex analysis, and structured
sparsity.

\section{Machine Learning with Hidden Representations of Language}
\label{sec:baseml}

We begin by providing a more rigorous and practical explanation of the machine
learning pipeline for NLP, shown in \figref{highlevel}.

Given an input prompt $x$ from a set of possible inputs $\mathcal{X}$
(for instance, the set of all possible English sentences),
we want to find the most likely output $y \in \mathcal{Y}$.
Various examples of objects we might be interested in predicting include

\begin{itemize}
\item the sentence's sentiment: $\mathcal{Y} = \{\mathsf{negative},
\mathsf{neutral}, \mathsf{positive} \}$
\item the writer's age: $\mathcal{Y} = \real_+$
\item the \emph{dependency tree} between the words in a sentence:
$\mathcal{Y}=\mathcal{T}$, \ie, the set of all directed trees with a single root.
\end{itemize}

We formalize this via a scoring function
which, given $x$, should assign higher scores to the correct output $y$ than to 
incorrect ones
\begin{equation}
\ss : \set{Y} \times \set{X} \to \real. 
\label{eqn:predict}
\end{equation}
We can make predictions by selecting the highest-scoring output
\[ \hat{y}(x) = \argmax_{\yy \in \mathcal{Y}} \ss(\yy; x), \]
and, where obvious from context, we may drop the argument and denote the
prediction simply as $\hat{y}$.

\paragraph{Supervised learning.}
Instead of designing the score $\ss$ entirely by hand, machine learning
entails learning a good model for $\ss$ based on \emph{training data}.
We thus assume access to a set of $N$ training inputs paired with desired outputs
\[ \mathcal{D} = \{ (x_i, y_i) \in \set{X} \times \set{Y} \}\big|_{i=1}^{N}.\]

To facilitate the exploration of the space of scoring functions $\sigma$,
let us consider \emph{parametrized} functions $\ss_{\weights}$, where
we denote by $\weights$ the parameter weights.
Then, we may pose the  learning problem as finding $\weights$ such that
$\ss_{\weights}$ matches the training data well.
Ideally, we would want to minimize the number of incorrect predictions
\begin{equation}
    \minz_{\weights} \sum_{i=1}^N \iverson[\hat{y}(x_i) \neq y_i],
\end{equation}
with $\hat{y}$ depending on $\ss_{\weights}$ as in \eqnref{predict}.
This discrete optimization problem is typically not tractable, so
a more amenable surrogate to the zero-one error is
considered. Denoting by $\s_{\weights}(x) \in \real^{|\mathcal{Y}|}$
the vector obtained by applying the scoring function to every possible
output $y$, we want to minimize
\begin{equation}
    \minz_{\weights} \sum_{i=1}^N \LL(\s_{\weights}(x), y_i).
    \label{eqn:risk}
\end{equation}

The key ingredient in \eqnref{risk} is the \textbf{loss function}
$\LL$, which measures the discrepancy between the scores and the true
object. Choosing a suitable loss function depends on many factors, including the
mathematical properties of the function, as well as the type of target objects
$y$; this subject will be discussed in more detail in
Chapter~\ref{chapter:losses}.
One example is the \emph{hinge loss} for classification
\begin{equation}
    \label{eqn:hingeclf}
    \LL(\s(x), y) = \max\left(0, 1 + \max_{y' \neq y}
    \ss(y'; x) - \ss(y; x)\right).
\end{equation}

\paragraph{Linear models.}
A simple way to define the function $\sigma_{\weights}$ is as a linear
function of the input.
This view requires a numeric representation of the data as vectors of features.
Usually, data is not provided directly in vector form, so practitioners must 
employ \textbf{feature extraction}. We may represent feature extraction as
a function $\bs{\phi}:\mathcal{X} \rightarrow \real^d$. Then, a linear model for
classification takes the form
\[ \ss_{\weights}(y; x) =
    \weights_y^\tr \bs{\phi}(x),
\]
where the weight vector is re-organized as the concatenation of per-class
weights, \ie, $\weights = [\weights_y]\big|_{y\in\mathcal{Y}}$.
Linear models are appealing because of their simplicity, and the ease of
optimizing $\weights$ in many situations. 

Typically, the feature extractor $g$ is defined by hand, by implementing it
programatically. For example, if $x$ is a sentence, the first feature
$\bigl(\phi(x)\bigr)_1$ may be defined as the number of words in $x$, and the
second feature $\bigl(\phi(x)\bigr)_2$ may be defined as $1$ if the last
character is a question mark, and $0$ otherwise. Practitioners dedicate plenty
of effort to finding good feature representations, in order to improve
predictive performance---an endeavor commonly known as \textbf{feature
engineering}.

\paragraph{Deep models.} Deep learning is a highly successful alternative to
feature engineering, where $\sigma$ can be represented as an arbitrary
composition of \emph{hidden layers}. To showcase why this is useful,
considering replacing the feature extractor $\bs{\phi}$ with a learnable
function $\bs{\phi}_{\weights}$, with the goal of learning appropriate feature
representations instead of having to engineer them manually.
We may write
\[ \ss_{\weights}(y; x) =
    \ss'_{\weights}(y; \bs{h}) \quad \text{where} \quad \bs{h} =
    \bs{\phi}_{\weights}(x), \]
thereby identifying $\bs{h} \in \real^d$ as a \emph{hidden} representation
of the input.
The remainder of the model $\ss'$ may be recursively defined through more
hidden layers, but deep models are not limited to \emph{sequential} composition:
an arbitrary \emph{computation graph} may be used to describe the operations to be
performed.  For instance, the input may be fed directly into deeper layers
(so-called \emph{skip connections}), and weights may be shared between layers
(leading to convolutional and recurrent networks, among others).
The computation graph abstraction is extremely powerful, due to the resulting
flexibility and modularity \citep{goodfellow}.

\paragraph{Backpropagation.} Unlike linear models, for which learning, \ie,
optimizing \eqnref{risk}, is relatively simple,
parameter learning in deep models can be more difficult. 
A popular approach is stochastic \emph{gradient-based} optimization. These
algorithms perform well empirically and make minimal assumptions about the model
$\s_{\weights}$: all that is needed is a way to compute the gradient of the
loss with respect to the model weights: \[ \pfrac{\LL(\s_{\weights}(x),
    y)}{\weights}. \] 
\emph{Backpropagation} is an algorithm for
evaluating this gradient at a given point, provided access to
\emph{Jacobian-vector} products at each computation node in the graph
\citep[Chapter 8.2]{nocedalwright}.
This allows researchers to develop neural network modules as
\textbf{building blocks} that practitioners may compose together in new and
creative ways: a programming paradigm that has proven very fruitful.

\section{Convex Analysis}
In this section, we recapitulate some useful definitions and results from the
theory of convex functions and sets, a crucial foundation for our results.

\paragraph{Definitions.}
A set $\mathcal{S}$ is called a \textbf{convex set} if it contains any segment
whose endpoints are in $\mathcal{S}$, in other words, if the following holds
\[
    \forall \bs{x}_1, \bs{x}_2 \in \mathcal{S}, \forall \alpha \in [0, 1], \qquad \alpha
    \bs{x}_1 + (1 - \alpha) \bs{x}_2 \in \mathcal{S}. 
\]
A function $f: \real^d \rightarrow \real$ is called a \textbf{convex function}
if $\dom f$ is convex and the following property holds
\[
    \forall \bs{x}_1, \bs{x}_2 \in \dom f, \forall \alpha \in [0, 1], \qquad
    f(\alpha \bs{x}_1 + (1 - \alpha) \bs{x}_2) \leq
    \alpha f(\bs{x}_1) + (1 - \alpha) f(\bs{x}_2).
\]

In the above, the pair $(\alpha, 1-\alpha)$ are called coefficients of a 
\textbf{convex combination}. More generally, a convex combination of $k$ points
$\bs{x}_1, \cdots, \bs{x}_k$ is the weighted average given by coefficients
$\bs{\alpha} = [\alpha_1, \cdots, \alpha_k] \in \Simplex^k$ as
\[ \sum_i \alpha_i \bs{x}_i = \bs{X\alpha}, \]
where $\bs{X}$ is a matrix whose columns are the $n$ points, \ie,
$\bs{X} = [\bs{x}_1^\tr, \cdots, \bs{x}_k^\tr].$

\paragraph{Convex hulls and polytopes.}
The convex hull of a set $\mathcal{S}$ is defined as the set of all convex
combinations of points in $\mathcal{S}$, \ie,
\[
    \conv \mathcal{S} = \left\{\sum_{i=1}^k \alpha_k \bs{x}_k :
    \bs{\alpha} \in \Simplex^k,  \bs{x}_i \in \mathcal{S}~ \forall i \in
    \range{k} \right\}.
\]
We use the term \textbf{polytope} to denote the convex hull of a finite set of
points. Given a polytope $\mathcal{P}$, there is a unique minimal set of points $\mathcal{V}$
such that $\mathcal{P} = \conv \mathcal{V}$, (minimal in the sense that $\forall
\mathcal{V}' \subsetneq \mathcal{V}~ \conv \mathcal{V}' \neq \mathcal{P}$).
The elements of $\mathcal{V}$ are called the \textbf{vertices} of $\mathcal{P}$.
The $k-1$-dimensional \textbf{canonical (probability) simplex} $\Simplex^k$ is the
polytope with the $k$ basis vectors $\bs{e}_1, \cdots, \bs{e}_k$ as vertices.
It follows that, any $k$-vertex polytope is the image of the
$\Simplex^k$ through the linear mapping given by $\bs{V}$
\[ \mathcal{P} = \{ \bs{V\alpha} : \alpha \in \Simplex^k \}, \]
where the columns $\bs{v}_1, \cdots, \bs{v}_k$ of $\bs{V}$ are the vertices of
$\mathcal{P}$.
Points in $\set{P}$ where at least one coordinate of $\bs{\alpha}$ is zero form
the \emph{relative boundary} of $\set{P}$, while all others form its
\emph{relative interior}:
\begin{equation}
    \label{eqn:relint}
    \relint \set{P} =  \{ \bs{V\alpha}: \bs{\alpha} \in \Simplex^k, \bs{\alpha}
    > \bs{0} \}.
\end{equation}

Any polytope can also be characterized as the solution set of a system of linear
equalities and inequalities; this view proves helpful when explicitly writing
optimality conditions of constrained optimization problems.

\paragraph{The convex conjugate.} Given a function $f: \real^d \rightarrow
\bar\real$, we define its conjugate, also known as its \emph{Legendre-Fenchel
transformation}, as
\begin{equation}
    \label{eqn:convexconj}
    f^\conj : \real^d \rightarrow \real \qquad f^\conj(\bs{y}) = \sup_{\bs{x} \in
    \dom f} \bs{y}^\tr\bs{x} - f(\bs{x}).
\end{equation}
The convex conjugate of a function is convex even when $f$ is not. An important
property of convex conjugates is the Fenchel-Young inequality~\citep{fenchel}
\begin{equation}
    \label{eqn:fenchelyoung}
    f(\bs{x}) + f^\conj(\bs{y}) \geq \bs{x}^\tr\bs{y}.
\end{equation}
\citet[Section 3.3.2]{boyd} provide more information and properties of the
convex conjugate.


\paragraph{Proximal operators.} Given a convex function $f: \real^d \rightarrow
\real$, its proximal operator is defined as \citep{parikhboyd}
\begin{equation}
    \prox{f} : \real^d \rightarrow \real^d \qquad
    \prox{f}(\bs{v}) = \argmin_{\bs{x}} f(\bs{x}) + \frac{1}{2} \norm{\bs{x} -
\bs{v}}_2^2 
\end{equation}
Under mild assumptions on $f$, this $\argmin$ is unique.
In particular, the proximal operator of the identity function of a convex set
$\mathcal{S} \subset \real^d$ is simply the euclidean projection onto $\mathcal{S}$
\begin{equation}
\begin{aligned}
    \prox{\id{\mathcal{S}}}(\bs{v}) 
    &= \argmin_{\bs{x} \in \real^d} \id{\mathcal{S}}(\bs{x}) + \frac{1}{2} \norm{\bs{x} - \bs{v}}_2^2
    &= \argmin_{\bs{x} \in \mathcal{S}} \norm{\bs{x} - \bs{v}}_2^2.
\end{aligned}
\end{equation}
This suggests an interpretation of proximal operators as
\emph{generalized projections}.
% The \textbf{Moreau decomposition} relates
% proximal operators with Fenchel conjugates
% \begin{equation}
    % \bs{v} = \prox{f}(\bs{v}) + \prox{f^\conj}(\bs{v}).
% \end{equation}


\section{Sparsity, Structured Sparsity, and Parsimony}
\label{sec:bg-sparsity}

The principle of \textbf{parsimony} states that, all things being equal,
a simple model should be preferred to a more complex one. Simplicity can
be defined in many ways, but generally, simple models are understood to be:

\begin{itemize}
    \item more computationally \textbf{efficient}, for instance via
        compact representations that require less storage;
    \item easier to \textbf{interpret} and visualize,
        since sparse explanations require less cognitive effort to reason about;
    \item more \textbf{plausible} in the presence of uncertainty and noise: even
        when we don't know for sure if the true phenomenon is simple, it may be
        a good idea to ``bet on simplicity''.\footnote{We are slightly paraphrasing
        the ``bet on sparsity'' as formulated by \citet{sls}: ``Use a procedure
        that does well in sparse problems, since no procedure does well in dense
        problems.''}
\end{itemize}

\textbf{Sparsity} is a typical measure of simplicity: a vector $\bs{x} \in \real^d$
is sparse if many of its coordinates are exactly zero. 
The number of non-zero coordinates of a vector is denoted as below, and can be
seen as a complexity measure.
\begin{equation}
    \norm{\bs{x}}_0 = \bigl| \{i \in \range{d} : x_i \neq 0 \}\bigr|
\end{equation}
Often called the $\ell_0$ norm by abuse of language, this function is not a
metric norm, but, if the domain is bounded, it is a limit of $\ell_p$ norms,
namely $\norm{\cdot}_0 = \lim_{p\to 0} \norm{\cdot}_p^p$.
The $\ell_0$ function is discontinuous
and non-convex, and optimization problems involving it are typically NP-hard.
However, other $p$-norms can be used to
\textbf{induce sparsity} while leading to simpler optimization problems.
The most commonly used such surrogate is the $\ell_1$ norm,
$\norm{\bs{x}}_1 = \sum_i |x_i|$. It is convex and continuous,
and minimizing or constraining its value yields sparse solutions.

To see why, we introduce the \textbf{unit ball} of an $\ell_p$ penalty function
\[
    \set{B}_{\norm{\cdot}_p} = \{ \bs{x} \in \real^d: \norm{\bs{x}}_p \leq 1 \}.
\]

\begin{figure}
    \centering
    \includegraphics[width=.66\textwidth]{fig/0202_lp_balls}
    \caption{$\ell_p$ norm balls $\mathcal{B}_{\norm{\cdot}_p}$ in a
        two-dimensional space, for several values of $p$. The legend matches the
        outside-to-inside ordering of the contours. For $p<1$, $\ell_p$
        are not proper norms; as $p\rightarrow0$ the limit vectors
        have exactly one nonzero coordinate.}
    \label{fig:0202-lp-balls}
\end{figure}

\figref{0202-lp-balls} illustrates unit balls for several interesting norms.
In particular, it can be seen that
$\mathcal{B}_{\norm{\cdot}_1}$ is a polytope with vertices $\{\pm \bs{e}_i : i
\in \range{d} \}$. The fundamental theorem of linear programming states that the
minimum of a linear function over a polytope (including
$\mathcal{B}_{\norm{\cdot}_1}$) is always attained at a vertex
\citep[Theorem 6]{dantzig}; similarly,
the minimum of a quadratic function over a polytope is
\emph{likely} (but not always) attained at a vertex. Both phenomena are
illustrated in \figref{0204-l1-sparsity}.
Since euclidean projections are quadratic minimizations, it follows that
$\proj{\mathcal{B}_{\norm{\cdot}_1}}$ has sparse solutions.
We further note that the $d$-simplex is a face of the $d$-dimensional $\ell_1$
ball, suggesting that similar optimization problems over the simplex also result
in sparse solutions; we later prove a more general result about what
probability mappings are sparsity-inducing in Proposition~\ref{prop:full_simplex}.

\begin{figure}
    \centering
    \includegraphics[width=.49\textwidth]{fig/0204_l1_sparsity_linear}
    \includegraphics[width=.49\textwidth]{fig/0205_l1_sparsity_quad}
    \caption{When optimizing a linear function (left) or a quadratic function
        (right) over the $\ell_1$ ball, solutions are likely sparse.}
     \label{fig:0204-l1-sparsity}
\end{figure}

\textbf{Vertex sparsity} is a notion of simplicity for points in a
polytope $\mathcal{P} = \conv \mathcal{V}$. Recall that any 
$\bs{x}\in\mathcal{P}$ can be represented as a convex combination of its
vertices: 
\begin{equation}
\forall \bs{x} \in \mathcal{P}
~\exists~
\bs{\alpha} \in \Simplex^{|\mathcal{V}|},
\quad
\bs{x} = \sum_{\bs{v} \in
\mathcal{V}} \alpha_v \bs{v}.
\label{eqn:vecrep}
\end{equation}

In some cases, for instance if all vertices are strictly positive vectors, \ie,
$\bs{v} \succ \bs{0}$, $\mathcal{P}$ may even contain no points with sparse
\emph{coordinates}. Yet, by shifting to the representation in \eqnref{vecrep},
we find that some points can be compactly represented as sparse \emph{convex
combinations} involving only a few vertices, \ie, $\norm{\bs{\alpha}}_0 \ll
|\mathcal{V}|$. This can be just as desirable as coordinate sparsity. Moreover,
if the vertices themselves are sparse vectors, then vertex sparsity results in
coordinate sparsity as well.\footnote{Note that when $\mathcal{P}=\Simplex$,
    coordinate sparsity and vertex sparsity coincide.}

\paragraph{Group sparsity.} 
In some cases, the coordinates of a vector $\bs{x} \in \real^d$ have some known
meaning, and we might want several coordinates to either all be zero or all be
nonzero. We may organize the indices into groups $\mathcal{G}_i \subset
\range{d}\big|_{i=1}^k.$ The \emph{group lasso} penalty \citep{grouplasso} is
defined as
\begin{equation*}
R_{\text{GL}}(\bs{x}) = \sum_{i=1}^k \norm{\bs{x}_{\mathcal{G}_i}}_2. 
\end{equation*}
If $k=d$ and each coordinate is in a separate group, \ie, $\mathcal{G}_i=\{i\}$,
then the group lasso reverts to the $\ell_1$ penalty discussed above.
While originally proposed for non-overlapping groups, the group lasso has been
extended to unions and intersections of overlapping groups
\citep{grouplasso-jacob,grouplasso-jenatton}.

\paragraph{Fusing values.} Parsimony can go beyond zeroing out coordinates,
vertices or groups. We may also encourage simplicity in a vector by
clustering (fusing) together its \emph{values}. An important instance of this is
the \textbf{fused lasso}, used to smoothen or denoise a 1-d sequence (represented
as a vector $\bm{x}$ by encouraging adjacent coefficients to be exactly equal.
This can be done by penalizing the absolute value of their difference
\citep{fused_lasso}
\begin{equation}
R_{\text{FL}}(\bs{x}) = \sum_{i=1}^{d-1} |x_{i+1} - x_{i}|.
\end{equation}

More generally, given a graph over the $d$ indices, weighted by a matrix $\bs{W}
\in \real^{d \times d}$, we may define a \emph{generalized} fused lasso penalty
\citep{gfl}
\begin{equation}
    R_{\text{GFL}}(\bs{x}) = \sum_{i<j} w_{ij} |x_{i} - x_{j}|.
\end{equation}
Unlike the 1-d version, for which efficient exact algorithms exist, the
general formulation is more challenging. 

In some cases, the actual ordering of the coefficients doesn't matter, but we
might still desire a parsimonious vector with only a few distinct values.
The \textbf{OSCAR} regularizer \citep{oscar} achieves this effect without
incurring the high computational cost of the generalized fused lasso.
\begin{equation}
    R_{\text{OSC}} = \lambda_1 \norm{\bs{x}} + \lambda_2 \sum_{i<j} \max(|x_i|, |x_j|).
    \label{eqn:oscar}
\end{equation}

\paragraph{Generalized penalties.}
There has been a lot of interest in the study of more generalized structured
norms and penalties. We briefly list some directions, useful not only in
hand-crafting better suited penalties for a task as hand, but also for their
analysis which can lead to insights into the penalties discussed above. 

The \textbf{ordered weighted \boldmath $\ell_1$ (OWL)} norm
\citep{owl_xeng_figueiredo15} is
\[
R_{\text{OWL}}(\bs{x}) = \sum_i w_i |x^\downarrow_i|, \] where $x^\downarrow_i$
denotes the $i$th largest element of $\bs{x}$. With the weight
defined as $w_i = 1$, OWL is equal to the $\ell_1$ norm; when $w_1 = 1$ and
$w_i = 0$ for $i > 1$ it amounts to $\ell_\infty$, and for $w_i = \lambda_1 +
\lambda_2(d-i)$, OWL becomes the OSCAR regularizer in \eqnref{oscar}.

Another perspective is given through \textbf{atomic norms}
\citep{atomic}. A set of \emph{atoms} $\mathcal{A}^k \subset \real^d$ induces,
under some assumptions, the norm
%compact, centrally-symmetric, and such that conv(A) contains a ball of radius
%epsilon: words from owl_xeng_figueiredo15.
\[ \norm{\bs{x}}_\mathcal{A} = \inf \{ t \geq 0 : \bs{x} \in t \conv
\mathcal{A} \}. \]
Atomic norms induce representations as sparse affine combinations of atoms.
For $\mathcal{A}=\{\pm \bs{e}_i:
i \in \range{d}\}, \norm{\cdot}_\mathcal{A} = \norm{\cdot}_1$; for
$\mathcal{A}= \{-1, 1\}^d, \norm{\cdot}_\mathcal{A} = \norm{\cdot}_\infty$.
An atomic formulation of the OWL norm
is given by \citet{owl_xeng_figueiredo15}.
