\chapter{Introduction}

In this chapter, we set the stage for presenting our contributions in context,
we outline our work, and describe the organization of the remaining chapters.

\section{Structure in Natural Language Processing}

Computational methods are increasingly common for tackling challenging natural
language processing (NLP) tasks.  Machine learning approaches, in particular,
are gaining success at a variety of challenging natural language problems.
A popular example is \emph{machine translation}, which takes as input a sentence
in a source language, and aims to produce as output a translated sentence in the
target language. Machine translation is deployed today at a large scale in successful commercial
products.

Among the wide variety of computational methods considered for NLP applications,
this thesis is focused on methods that identify and extract linguistic
\emph{structure}, in other words, discrete combinatorial representations of 
a text, reflecting the underlying linguistic phenomena at work.
For decades, linguists have studied the tangled network
of structural representations, manifesting at different scales. For example,
through syntactic analysis, a sentence can be organized as a tree of
\emph{constituent} chunks, as formalized by \citet{chomsky56}; alternatively,
\emph{dependency} analysis yields a different kind of tree representation, where
each word is a node, and arcs represent direct relations of grammatical
dependency, a view deriving from the work of \citet{tesniere}.
At the document level, the perspective shifts to larger-scale structures, for
example coreference, discourse, and argumentation.  Between multiple texts, we
may be interested in \emph{alignment} structures \citep{bitext}.

For machine learning systems applied to NLP tasks, the input typically consists
of text, while the output depends on the task of interest. Furthermore,
many so-called \emph{deep} models involve hidden (latent) representations
computed along the way. A bird's eye view of a simple deep model for
predicting the sentiment of a sentence is depicted in \figref{highlevel-unstruct};
the hidden representation depicted is, for instance, the dense vector output of a
hidden layer inside a neural network.

\input{tikz/0100-highlevel.tikz.tex}
\begin{figure}[h]
    \centering
    \subcaptionbox{\label{fig:highlevel-unstruct}no structure}{%
    \usebox{\cartoonnormal}} \\[2em]
    \subcaptionbox{\label{fig:highlevel-structout}structured output}{%
    \usebox{\cartoonstructout}} \\[2em]
    \subcaptionbox{\label{fig:highlevel-structlatent}latent structure}{%
    \usebox{\cartoonstructlatent}} \\
\caption{\label{fig:highlevel}%
    High-level view of \emph{deep} machine learning models for NLP, with a hidden
    representation $\bm{h}$ emphasized:
    (a) a vanilla sentiment classifier, with unstructured hidden layers
    (typically, vector representations consisting of real numbers);
    (b) a structured-output model (\eg, a parser);
    (c) a deep model with latent structure.}
\end{figure}

Linguistic structure may play three main roles within a machine learning model:
\begin{itemize}
    \item \textbf{Structured input.} In some circumstances, the input can
        be given with structural annotations; for instance, if users are
        required to input their text in a structured web-form, or if
        expert human annotators are employed.  Structured input data is typically
        handled in a preprocessing or \emph{feature extraction} step, and
        recent work employs neural models for graph inputs \citep[among others]{graphcnn,graphtoseq}.
        While they represent a promising research direction,
        structured \emph{input} models are out of the scope of our work.
    \item \textbf{Structured output} (\figref{highlevel-structout}). Instead of
        picking a category from a short
        list, the desired output itself might be a structured representation;
        for example, the most likely dependency parse tree of the given
        sentence.  Structured output prediction~\citep{bakir}, especially in NLP
        \citep{noahbook}, 
        is characterized by highly expressive models, able to handle
        constraints and correlations at both a local level (for instance,
        which tag assignments are preferred or allowed for a given word) 
        as well as at a global level (for instance, certain joint assignments may
        be disallowed). As such, finding the highest-scoring structure can be
        technically challenging.
    \item \textbf{Latent structure} (\figref{highlevel-structlatent}). In deep models,
        even if the desired output
        is unstructured, extracting \textbf{structured hidden representations}
        can potentially be beneficial for the downstream task; for example,
        taking a guess at the dependency structure of a sentence can help deal
        with scoped linguistic phenomena such as negation, leading to more
        accurate sentiment predictions. Latent structure inherits all the
        challenges of structured prediction while facing additional ones,
        essentially due to the tension between discrete choices and gradient
        backpropagation, as we shall discuss in more detail in
        \chapref{sparseattn}. Mitigating this tension through \emph{sparsity} is
        a running theme in this dissertation.

\end{itemize}

In this work, we explore \textbf{structured output prediction} and
\textbf{latent structure}, pushing the boundaries of model expressiveness,
performance, and generality. We apply our approaches to a wide range
of tasks, including machine translation, dependency parsing, natural language
inference, and argument mining.

\section{Contributions}

\paragraph{Structured and sparse attention mechanisms via regularization.}

Neural attention is a recently developed mechanism for assigning latent
probability weights to items (often, words within a sentence). We uncover a new perspective that casts attention
mechanisms in terms of regularized \emph{max} operators, leading to new
derivations of well-known unstructured attention mechanisms.
By drawing from extensive research on structured sparsity, our framework allows
us to derive new attention mappings, which may encode structural priors. 
For example, in many languages, coherent phrases consist of adjacent words; thus
we develop \textbf{fusedmax}: a linguistically-motivated attention mechanism
tending to \emph{group adjacent words together}. Since in some languages word
order is variable, we also develop \textbf{oscarmax}, a mechanism that may
\emph{cluster non-adjacent words} as well.  We showcase our proposed methods on
sentence summarization, machine translation, and natural language inference,
yielding superior interpretability with competitive performance and
computational cost compared to traditional unstructured dense attention.

\paragraph{Differentiable sparse structured inference.}

For more complicated globally constrained structures, such as \emph{matchings}
or \emph{dependency trees}, we turn to the framework of structured inference in
probabilistic graphical models \citep{wainwright}.  In particular, to tackle the
challenge of searching over the enormous number of possible structures, we
introduce {\smap}, a new inference strategy.  {\smap} inference is able to
automatically select only a few global structures: it is situated between
\emph{maximum a posteriori} (MAP) inference, which picks a single structure, and
marginal inference, which assigns probability mass to all structures, including
implausible ones.  Importantly, {\smap} can be computed using only calls to a
MAP oracle, hence it is applicable even to problems where marginal inference is
intractable,  such as the linear assignment (or matching) problem.  Sparsity
makes gradient backpropagation efficient regardless of the structure, enabling
us to augment deep neural networks with generic and sparse {\bf structured
hidden layers}.  Experiments in natural language inference reveal competitive
accuracy and improved interpretability when compared to unstructured mechanisms.

\paragraph{Latent neural network structure.}

Deep NLP models benefit from adapting their computation to underlying structures
in the data; \eg, TreeLSTMs using syntax as a hierarchical composition order. Yet,
the structure is typically extracted using off-the-shelf parsers. Recent attempts
to jointly learn the latent structure encounter a trade-off: they can either make
factorization assumptions that limit expressiveness, or sacrifice end-to-end
differentiability.
Using our novel \smap inference, which retrieves a sparse distribution over
latent structures, we propose a novel approach for end-to-end learning of
latent structure predictors jointly with a downstream predictor. Our method
enables unrestricted dynamic computation graph construction from the
\emph{global} latent structure, while maintaining differentiability.
This approach leads to improved performance on tasks such as sentiment
classification, natural language inference, and reverse dictionary lookup.

\paragraph{Structured Fenchel-Young Losses and {\boldmath \smap} losses.}

We derive a family of structured losses encompassing the conditional random
field (CRF), the structured perceptron, and the structured SVM losses. We
analyze some useful properties of this family, and use it to derive novel losses
based on \smap inference.  Exploiting the sparse distribution over structures
produced by \smap is valuable in practice, as we demonstrate on dependency
parsing, where we outperform the aforementioned losses on most languages
considered. Our parsers get increasingly sparser as training progresses, peaking
on a single tree for unambiguous sentences. On sentences with inherent
linguistic ambiguity, \smap parsers retrieve a small set of candidate parse
trees, helping both practitioners and downstream applications in pipeline
systems.

\paragraph{Expressive neural structured models for argument mining.}

To validate the importance of incorporating structure and domain knowledge
in specialized NLP applications, we study in greater detail one specific
application, \emph{argument mining}, whose goal is to extract argumentation
structures from documents.
We construct an expressive graphical
model, capturing the domain knowledge present in two different but related
argumentation datasets.  Structured output prediction techniques enable our
model to jointly learn elementary unit type classification and argumentative
relation prediction, capturing correlations between adjacent relations as well
as global constraints. Experimental results reveal that global structured models are
essential for argument mining, outperforming unstructured baselines.

\section{Roadmap}

The remainder of this thesis is organized as follows.

We begin by reviewing, in Chapter~\ref{chapter:bg}, the relevant background in
neural network models for NLP, convex analysis, and structured sparsity.

Chapters~\ref{chapter:sparseattn}, \ref{chapter:sparsemap}, and
\ref{chapter:latent} study \textbf{latent structure} in neural networks.
In Chapter~\ref{chapter:sparseattn}, we develop a generalized framework
for \emph{neural attention mechanisms} based on regularization, allowing us to
develop differentiable attention mechanisms that enforce structured sparsity.
In Chapter~\ref{chapter:sparsemap} we propose \smap, a novel strategy for
differentiable sparse structured inference, with efficient algorithms for
computing the forward and backward passes provided access to MAP inference.
Next, in Chapter~\ref{chapter:latent}, we develop a method for using \smap
to train neural networks whose computation graphs depend freely and directly on
structured latent variables.

Chapters~\ref{chapter:losses} and \ref{chapter:marseille} are concerned with
\textbf{structured output prediction}. In Chapter~\ref{chapter:losses} we
introduce a family of structured losses generalizing the most commonly used
ones. We propose \smap losses as members of this family and explore their
theoretical and practical properties and performance. In
Chapter~\ref{chapter:marseille} we design and evaluate a powerful structured
output model for argumentation mining, reaffirming the importance of
incorporating structure and domain knowledge in NLP models.

We conclude in Chapter~\ref{chapter:conclusions} by contextualizing our
contributions and the future work envisioned in the current landscape of
structured models for NLP.
